# 卸载资源逻辑

bundle_assets这个字典中会保存每一个bundle加载了哪些资源，
当卸载资源时，会把资源从bundle_assets字典中移除，移除之后进行检测当前bundle中是否还有资源，
如果没有资源，则会判断当前这个bundle是不是被其他的bundle依赖，
如果没有被别人依赖则会把这个bundle卸载掉!


## 怎样判断bundle的依赖？

遍历所有已加载的bundle,获取bundle的依赖，判断依赖的bundle中是否包含当前的bundle！



## 场景的卸载

AssetBundleManager会在初始化的方法中监听场景卸载的事件，当监听到某一个场景被卸载时，会调用卸载的方法
