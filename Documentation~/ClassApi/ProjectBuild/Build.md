# ProjectBuild.Build



### 方法:

public static void Build(XFABProject project, BuildTarget buildTarget,bool revealInFinder = true);

### 说明:

打包某一个模块的AssetBundle(仅可在Editor模式下调用)

和点击下图中的按钮效果一样:

![](textures/build.jpeg)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| project      | 资源模块        |
| buildTarget      | 目标平台        |
| revealInFinder      | 打包完成后是否打开AssetBundle所在的文件夹(默认:true)        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class BuildExample : MonoBehaviour
>{
>    void Start()
>    {
>#if UNITY_EDITOR
>        // 查询到Test模块
>        XFABProject project = XFABProjectManager.Instance.GetProject("Test");
>        // 打包Test模块的AssetBundle
>        ProjectBuild.Build(project, UnityEditor.BuildTarget.Android, false);
>#endif
>    }
>}
> ```