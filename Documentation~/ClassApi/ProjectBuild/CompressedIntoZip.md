# ProjectBuild.CompressedIntoZip



### 方法:

public static bool CompressedIntoZip(XFABProject project,BuildTarget buildTarget);

### 说明:

把打包完成的AssetBundle文件压缩成zip(仅可在Editor模式下调用)

和点击下图中的按钮效果一样:

![](textures/zip.jpeg)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| project      | 资源模块        |
| buildTarget      | 目标平台        |


### 返回值

类型 : **boolean** 

字段介绍 : 

true:压缩成功 false:压缩失败

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CompressedIntoZipExample : MonoBehaviour
>{
>    void Start()
>    {
>#if UNITY_EDITOR
>        // 查询到Test模块
>        XFABProject project = XFABProjectManager.Instance.GetProject("Test");
>        // 把打包完成的AssetBundle文件压缩成zip
>        ProjectBuild.CompressedIntoZip(project, UnityEditor.BuildTarget.Android);
>#endif
>    }
>}
> ```
