# ProjectBuild.PackAtlas



### 方法:

public static void PackAtlas(XFABProject project);

### 说明:

打包某一个资源模块的图集(仅可在Editor模式下调用)

和点击下图中的按钮效果一样:

![](textures/package.jpeg)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| project      | 资源模块        | 
 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class PackAtlasExample : MonoBehaviour
>{
>    void Start()
>    {
>#if UNITY_EDITOR
>        // 查询到Test模块
>        XFABProject project = XFABProjectManager.Instance.GetProject("Test");
>        // 打包Test模块的图集
>        ProjectBuild.PackAtlas(project );
>#endif
>    }
>}
> ```