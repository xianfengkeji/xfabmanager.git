# DateTimeTools
class in XFABManager

### 说明:


日期工具


### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [DateTimeToTimestamp](DateTimeTools/DateTimeToTimestamp.md)      | 本时区日期时间转时间戳        |
| [TimestampToDateTime](DateTimeTools/TimestampToDateTime.md)      | 时间戳转本时区日期时间        |