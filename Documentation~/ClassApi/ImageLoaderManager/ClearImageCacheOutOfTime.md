# ImageLoaderManager.ClearImageCacheOutOfTime



### 属性:

public static int ClearImageCacheOutOfTime;

### 说明:

当缓存到本地的图片超过多少时间会被删除 (单位:天)

默认超过 30 天会被删除 最小:1 最大:365
 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ClearImageCacheOutOfTimeExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 本地缓存清理的时间，默认超过30天会被删除
>        Debug.LogFormat("本地缓存清理的时间:{0}", ImageLoaderManager.ClearImageCacheOutOfTime);
>        // 设置为超过10天删除
>        ImageLoaderManager.ClearImageCacheOutOfTime = 10; 
>    }
>}
> ```