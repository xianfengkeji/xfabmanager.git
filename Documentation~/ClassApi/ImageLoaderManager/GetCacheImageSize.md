# ImageLoaderManager.GetCacheImageSize



### 方法:

public static long GetCacheImageSize();

### 说明:

获取本地所有缓存图片的总大小(当ImageLoader从网络加载图片时,会缓存到本地)(单位:字节)

 
### 返回值

类型 : **long**  

字段介绍 : 

缓存图片的总大小(单位:字节)

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetCacheImageSizeExample : MonoBehaviour
>{
>    void Start()
>    { 
>        Debug.LogFormat("本地缓存图片总大小:{0} byte", ImageLoaderManager.GetCacheImageSize()); 
>    }
>}
> ```