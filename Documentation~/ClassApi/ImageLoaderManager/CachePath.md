# ImageLoaderManager.CachePath



### 属性:

public static string CachePath;

### 说明:

通过ImageLoader请求网络图片在本地的缓存文件夹路径

 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CachePathExample : MonoBehaviour
>{
>    void Start()
>    {
>        Debug.LogFormat("缓存文件夹路径:{0}", ImageLoaderManager.CachePath);
>    }
>}
> ```