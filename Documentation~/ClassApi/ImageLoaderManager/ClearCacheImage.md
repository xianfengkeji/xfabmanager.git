# ImageLoaderManager.ClearCacheImage



### 方法:

public static void ClearCacheImage();

### 说明:

清空从网络缓存的图片


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ClearCacheImageExample : MonoBehaviour
>{
>    void Start()
>    { 
>        // 清空本地的缓存图片
>        ImageLoaderManager.ClearCacheImage();
>    }
>}
> ```
