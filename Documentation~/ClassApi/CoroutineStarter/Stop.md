# CoroutineStarter.Stop



### 方法:

public static void **Stop**(Coroutine coroutine);

### 说明:

停止由CoroutineStarter启动的协程 


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| coroutine      | 想要停止的协程对象        |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class StopExample : MonoBehaviour
>{
>    private void Start()
>    {
>        // 启动协程（适用于在非继承自MonoBehaviour的脚本中启动协程）
>        Coroutine coroutine = CoroutineStarter.Start(Test());
>        // 停止协程
>        CoroutineStarter.Stop(coroutine);
>    }
>
>    IEnumerator Test() { 
>        yield return new WaitForSeconds(1);
>        Debug.Log("Test Coroutine!");
>    }
>}
> ```