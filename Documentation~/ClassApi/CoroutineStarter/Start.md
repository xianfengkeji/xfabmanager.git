# CoroutineStarter.Start



### 方法:

public static Coroutine Start(IEnumerator enumerator);

### 说明:

启动协程（适用于在非继承自MonoBehaviour的脚本中启动协程）,且通过该方法启动的协程不会因为切换场景而停止


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| enumerator      | 需要启动的枚举器        |

### 返回值

类型 : **Coroutine**     **[可参考官方介绍](https://docs.unity3d.com/ScriptReference/Coroutine.html)**
 
### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class StartExample : MonoBehaviour
>{
>    private void Start()
>    {
>        // 启动协程（适用于在非继承自MonoBehaviour的脚本中启动协程）,且通过该方法启动的协程不会因为切换场景而停止
>        CoroutineStarter.Start(Test());
>    }
>
>    IEnumerator Test() { 
>        yield return new WaitForSeconds(1);
>        Debug.Log("Test Coroutine!");
>    }
>}
> ```
 