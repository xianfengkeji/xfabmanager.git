# ZipTools.UnZipFile



### 方法:

public static bool UnZipFile(string zipFilePath,string targetDirectory);

### 说明:

解压zip文件


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| zipFilePath      | 需要解压的.zip路径        |
| targetDirectory      | 解压后的文件存放路径        |

### 返回值

类型 : **boolean**  

字段介绍 :  true:解压成功 false:解压失败

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UnZipFileExample : MonoBehaviour
>{
>    void Start()
>    {  
>        // 解压C:/test.zip文件
>        bool success = ZipTools.UnZipFile("C:/test.zip", "C:/test");
>        Debug.LogFormat("是否解压成功:{0}", success);
>    }
>}
> ```