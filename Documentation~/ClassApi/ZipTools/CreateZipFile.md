# ZipTools.CreateZipFile



### 方法:

public static bool CreateZipFile(string dirPath, string zipFilePath);

### 说明:

压缩文件


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| dirPath      | 需要压缩的文件夹路径        |
| zipFilePath      | 压缩后的.zip文件路径        |

### 返回值

类型 : **boolean**  

字段介绍 :  true:压缩成功 false:压缩失败

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CreateZipFileExample : MonoBehaviour
>{
>    void Start()
>    { 
>        // 压缩C:/test文件夹
>        bool success = ZipTools.CreateZipFile("C:/test", "C:/test.zip");
>        Debug.LogFormat("是否压缩成功:{0}",success);
>    }
>}
> ```