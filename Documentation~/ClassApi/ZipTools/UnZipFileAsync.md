# ZipTools.UnZipFileAsync



### 方法:

public static async Task<bool> UnZipFileAsync(string zipFilePath, string targetDirectory,Action<float> progress = null);

### 说明:

异步解压zip文件


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| zipFilePath      | 需要解压的.zip路径        |
| targetDirectory      | 解压后的文件存放路径        |
| progress      | 解压的进度回调        |

### 返回值

类型 : **Task<bool>**  

字段介绍 :  true:解压成功 false:解压失败

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UnZipFileExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {    
>        UnityEngine.Debug.Log("开始解压");  
>        float progress = 0;
>        // 解压C:/test.zip文件
>        Task<bool> result = ZipTools.UnZipFileAsync("C:/test.zip", "C:/test", (p) =>
>        {
>            progress = p;
>        });
>        while (!result.IsCompleted)
>        {
>            yield return null;
>            UnityEngine.Debug.LogFormat("解压进度:{0}",progress);
>        }
>        UnityEngine.Debug.LogFormat("是否解压成功:{0} ", result.Result);
>    }
>}
> ```