# XFABProjectManager
class in XFABManagerEditor

### 说明:

资源模块管理类


### 属性

| 名称      | 说明 |
| ----------- | ----------- |
|  [Projects](XFABProjectManager/Projects.md)  | 所有资源模块集合       | 

### 静态属性

| 名称      | 说明 |
| ----------- | ----------- |
|  Instance  | 单例       | 


### 方法

| 名称      | 说明 |
| ----------- | ----------- |
|  [RefreshProjects](XFABProjectManager/RefreshProjects.md)  | 刷新项目列表       | 
|  [IsContainProject](XFABProjectManager/IsContainProject.md)  | 判断是否包含某个项目       | 
|  [GetProject](XFABProjectManager/GetProject.md)  | 查询项目       | 
|  [IsHaveProjectDependence](XFABProjectManager/IsHaveProjectDependence.md)  | 是否有项目依赖这个项目       | 