# ImageLoaderManager
class in XFABManager

### 说明:

ImageLoader帮助类

### 静态属性

| 名称      | 说明 |
| ----------- | ----------- |
|  [CachePath](ImageLoaderManager/CachePath.md)  | 通过ImageLoader请求网络图片在本地的缓存文件夹路径       | 
|  [ClearImageCacheOutOfTime](ImageLoaderManager/ClearImageCacheOutOfTime.md)  | 当缓存到本地的图片超过多少时间会被删除 (单位:天) 默认超过 30 天会被删除 最小:1 最大:365      | 


### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [GetCacheImageSize](ImageLoaderManager/GetCacheImageSize.md)      | 获取本地所有缓存图片的总大小( 当ImageLoader从网络加载图片时,会缓存到本地 )        |
| [ClearCacheImage](ImageLoaderManager/ClearCacheImage.md)      | 清空从网络缓存的图片      |