# AssetBundleTools
class in XFABManager

### 说明:

AssetBundle工具类

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [IsValidAssetBundleFile](AssetBundleTools/IsValidAssetBundleFile.md)      | 判断一个资源文件是否能够打包进AssetBundle(仅在编辑器模式可用)       |
| [IsNeedsToBePackagedSeparately](AssetBundleTools/IsNeedsToBePackagedSeparately.md)      | 判断一个资源是不是需要单独打包(仅在编辑器模式可用) | 