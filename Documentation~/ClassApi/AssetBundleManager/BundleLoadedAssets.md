# AssetBundleManager.BundleLoadedAssets

public static Dictionary<string, Dictionary<string, Dictionary<int, object>>> BundleLoadedAssets;


### 说明:

该属性记录了某个AssetBundle中加载了哪些资源 . 只读

key ： 类型 string  资源模块名称

value : 类型 Dictionary<string, Dictionary<int, object>>  
        
        key : AssetBundle名称 
        
        value: Dictionary<int, object>  key : 类型 int 已加载的资源的hashcode , value  资源对象


### 用法如下:

> ```none
>
>using UnityEngine;
>
>public class BundleLoadedAssetsExample : MonoBehaviour
>{
>
>    void Start()
>    {
>       foreach (var projectName in AssetBundleManager.BundleLoadedAssets.Keys)
>       {
>           Dictionary<string, Dictionary<int,object>> bundles = AssetBundleManager.BundleLoadedAssets[projectName];
>           Debug.LogFormat("资源模块:{0} 已加载 AssetBundle 数量:{1}", projectName, bundles.Count);
>           foreach (var bundleName in bundles.Keys)
>           {
>               Debug.LogFormat("资源包名:{0} 已加载资源数量:{1}", bundleName, bundles[bundleName].Count);
>           }
>       }
>    }
>}
> ```
