# AssetBundleManager.ExtractRes



### 方法:

public static ExtractResRequest ExtractRes(string projectName);

### 说明:

释放某个模块的资源 从内置目录(StreamingAssets) 复制到 数据目录(persistentDataPath)  

仅释放当前项目资源 不包含其依赖项目


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **ExtractResRequest**     **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ExtractResExample : MonoBehaviour
>{
>   IEnumerator Start()
>   {
>       ExtractResRequest request = AssetBundleManager.ExtractRes("projectName");
>       yield return request;
>       if (string.IsNullOrEmpty(request.error))
>       {
>           // 资源释放成功
>       }
>       else {
>           Debug.LogFormat("资源释放失败:{0}",request.error);
>       }
>   }
>}
> ```


### 重载方法:


public static ExtractResRequest ExtractRes(CheckUpdateResult result);

### 说明:


根据检测结果来释放某个模块的资源

需要开发者先检测资源,如果检测的结果为释放资源 UpdateType.ExtractLocal ， 则可以调用此方法释放!

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| result      | CheckUpdateResult 检测更新的结果        |
