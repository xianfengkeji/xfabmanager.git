# AssetBundleManager.ReadyRes



### 方法:

public static ReadyResRequest ReadyRes(string projectName);

### 说明:

该方法的功能为: 准备某个模块的资源 , 在准备资源时会首先检测当前模块的资源需要做什么样的处理,然后根据检测的结果处理相应的逻辑,当准备资源成功后代表该模块
资源已经处于可用 或 最新状态. 推荐大家使用该方法, 使用该方法的优点就是不需要自己考虑当前模块的资源是需要 更新 释放 还是 下载 等逻辑, 只需要调用 ReadyRes 即可!


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 需要准备的资源模块名称        |

### 返回值

类型 : **ReadyResRequest**

说明:

**ReadyResRequest** 继承自 **CustomAsyncOperation** ， 是**可挂起**的对象 , 也就是说如果在**协程**中 , 可做如下操作:

> ```none
>   IEnumerator Test() 
>   {
>       // 开始准备资源
>       ReadyResRequest request = AssetBundleManager.ReadyRes("Test");
>       // 资源准备中  ( 协程处于挂起状态 直到资源准备完成 )
>       yield return request;
>       // 资源准备完成
>       if (string.IsNullOrEmpty(request.error))
>       {
>           Debug.Log("资源准备成功!");
>       }
>       else {
>           Debug.LogFormat("资源准备失败, 原因:{0}",request.error);
>       }
>   }
> ```


字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| ExecutionType      | 当前正在执行什么操作 比如:下载文件,解压文件,校验文件等  |
| CurrentProjectName      | 当前正在 准备的模块的名称 |
| CurrentSpeed      | 下载速度 单位:字节 |
| DownloadedSize      |  已下载的文件大小 单位:字节 |
| NeedDownloadedSize      |  需要下载的大小 单位:字节 |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ReadyResExample : MonoBehaviour
>{
>
>    private IEnumerator Start()
>    {
>        // 准备 Test 模块资源
>        ReadyResRequest request = AssetBundleManager.ReadyRes("Test");
>        
>        // 等待资源准备完成
>        yield return request;
>
>        // 如果想获取加载进度 或者 其他的加载中的信息可以采用下面方式 
>        //while (!request.isDone)
>        //{
>        //    yield return null;
>        //    Debug.LogFormat("进度:{0} 执行的类型:{1}", request.progress, request.ExecutionType);
>        //} 
>        if (string.IsNullOrEmpty(request.error)) {
>            // 资源准备成功 可以执行加载资源等操作了 TODO  
>        }
>        else{
>            Debug.LogFormat("资源加载失败了,原因:{0}",request.error);
>        }
>
>    }
>}
> ```


### 重载方法:


public static ReadyResRequest ReadyRes(CheckUpdateResult[] results);

### 说明:


该方法是通过检测更新的结果来准备资源, 而不是资源名, 使用需要开发者自己先[检测资源](CheckResUpdates.md)，检测成功之后拿到检测的结果再调用该方法进行准备!
适用场景, 在资源更新的流程中，当需要更新 或者 下载资源的时候，往往需要给用户 或 玩家提示更新的信息，比如需要更新的资源包大小，
更新的内容 等信息，让玩家确认是否更新，当玩家确认更新之后再进行准备资源的流程!


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| results      | CheckUpdateResult[] 检测更新的结果的数组        |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ReadyResExample : MonoBehaviour
>{
>    private IEnumerator Start()
>    {
>        // 检测更新
>        CheckResUpdateRequest check_request = AssetBundleManager.CheckResUpdate("Test");
>        yield return check_request;
>        if (string.IsNullOrEmpty(check_request.error)) 
>        {
>            // 资源检测成功
>            // 根据检测的结果做一些提示 TODO 
>            // 准备资源 
>            ReadyResRequest ready_request = AssetBundleManager.ReadyRes(new CheckUpdateResult[] { check_request.result });
>            yield return ready_request;
>            if (string.IsNullOrEmpty(ready_request.error))
>            {
>                // 资源准备成功 TODO
>            }
>            else {
>                Debug.LogFormat("资源准备失败:{0}", ready_request.error);
>            }
>        }
>        else
>        {
>            Debug.LogFormat("资源检测失败:{0}",check_request.error);    
>        } 
>    }
>}
> ```


### 重载方法:

public static ReadyResRequest ReadyRes(CheckUpdateResult result) ;

### 说明:

根据检测结果准备资源

与上面的重载函数功能一样,只不过参数不是数组

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| result      | 检测更新的结果      |
