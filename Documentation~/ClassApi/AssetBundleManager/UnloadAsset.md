# AssetBundleManager.UnloadAsset



### 方法:

public static void UnloadAsset(object asset);

### 说明:

卸载资源(**如果卸载场景请传入UnityEngine.SceneManagement.Scene**)

如果该资源所在的AssetBundle没有加载其他的资源 并且 该AssetBundle没有被其他的AssetBundle依赖,则会直接卸载AssetBundle

*注:在调用该方法之前,请确保要卸载的资源已不在使用,否则可能会导致资源异常!
使用场景:可用于明确知道某个资源是否正在使用,当不在使用时可调用此方法进行卸载
例如:有一个UIManager,所有的UI都是通过UIManager来加载,那此时 UIManager 可以做一个
引用计数,当某一个UI不在使用时,可通过此方法进行卸载,但是需要注意的是
该 UI 资源 不能通过 UIManager 以外的任何方式进行加载, 因为一旦加载,这个引用是
没有记录到UIManager中的,当UIManager 把这个资源卸载时,通过UIManager以外的方式
加载的资源将无法正常使用!


总的来说,如果需要卸载资源,被卸载的资源一定要统一管理, 避免有多种加载方式!

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| asset      | 需要卸载的资源对象 **如果卸载场景请传入UnityEngine.SceneManagement.Scene**        |
 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UnloadAssetExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 加载资源
>        GameObject prefab = AssetBundleManager.LoadAsset<GameObject>("prejectName", "assetName");
>        // 如果prefab 不在使用 可以通过下面的方式卸载
>        AssetBundleManager.UnloadAsset(prefab);
>        // 加载资源有一个特殊的情况就是场景 场景是没有返回值的 如果卸载场景可参考下面的方式:
>        AssetBundleManager.LoadScene("projectName", "sceneName", UnityEngine.SceneManagement.LoadSceneMode.Single);
>        // 卸载场景资源
>        Scene scene = SceneManager.GetSceneByName("sceneName");
>        AssetBundleManager.UnloadAsset(scene);
>    }
>}
> ```
