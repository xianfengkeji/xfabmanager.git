# AssetBundleManager.GetAssetBundleFilesSize


### 方法:

public static long GetAssetBundleFilesSize(string projectName);

### 说明:

获取某一个模块的所有AssetBundle文件大小 单位:字节


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **long** 


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetAssetBundleFilesSizeExample : MonoBehaviour
>{
>    void Start()
>    {
>        long size = AssetBundleManager.GetAssetBundleFilesSize("projectName");
>        Debug.LogFormat("AssetBundle文件大小为:{0}:mb",size / 1024f/1024f);
>    }
>}
> ```
