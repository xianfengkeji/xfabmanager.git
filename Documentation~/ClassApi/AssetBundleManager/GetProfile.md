# AssetBundleManager.GetProfile



### 方法:

public static Profile GetProfile(string projectName = "");

### 说明:

获取某一个模块的配置信息 (如果没有单独设置则返回默认配置)

**可在下图配置或修改**
![](texture/profile.jpg)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称，默认为空,如果为空则返回默认配置        |

### 返回值

类型 : **Profile** 

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| name      | 配置名称       |
| url      | 资源更新地址       |
| updateModel      | 更新模式  |
| loadMode      |加载模式 默认 Assets， Asset 模式仅在Editor模式有用，非Editor模式无论怎么设置都是AssetBundle   |
| useDefaultGetProjectVersion      |  是否使用默认的获取项目版本方式，已过时,可忽略    |
| ProjectName      | 当前这个配置是针对那个资源模块的   |
| GroupName      | 当前这个配置属于哪个组    | 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetProfileExample : MonoBehaviour
>{
>    void Start()
>    {
>        Profile profile = AssetBundleManager.GetProfile("projectName");
>        Debug.LogFormat("配置信息:{0}",JsonUtility.ToJson(profile));
>    }
>}
> ```