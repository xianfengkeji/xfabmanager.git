# AssetBundleManager.GetAssetBundleSuffix



### 方法:

public static string GetAssetBundleSuffix(string projectName);

### 说明:

获取某个模块 AssetBundle 的后缀名


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **string**

字段介绍 : 后缀名称

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetAssetBundleSuffixExample : MonoBehaviour
>{
>    void Start()
>    {
>        string suffix = AssetBundleManager.GetAssetBundleSuffix("projectName");
>        Debug.LogFormat("AssetBundle 后缀:{0}",suffix);
>    }
>}
> ```