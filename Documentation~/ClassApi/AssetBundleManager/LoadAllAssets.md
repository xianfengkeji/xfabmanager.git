# AssetBundleManager.LoadAllAssets



### 方法:

public static UnityEngine.Object[] LoadAllAssets(string projectName, string bundleName)

### 说明:

加载某个AssetBundle的所有资源


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| bundleName      | 资源包名称        |

### 返回值

类型 : **UnityEngine.Object[]**  

资源对象数组

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAllAssetsExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 加载某个bundle中所有的资源
>        Object[] assets = AssetBundleManager.LoadAllAssets("projectName", "bundleName");
>        foreach (var asset in assets)
>        {
>            Debug.LogFormat("资源名称:{0}",asset.name);
>        }
>    }
>}
> ```


### 重载方法:


public static T[] LoadAllAssets<*T*>(string projectName, string bundleName) where T : UnityEngine.Object;

### 说明:


加载某个资源包中类型为T的所有资源

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 想要加载的资源类型        |
| projectName      | 资源模块名称        |
| bundleName      | 资源包名称        |


### 重载方法:


public static UnityEngine.Object[] LoadAllAssets(string projectName, string bundleName, Type type);

### 说明:


加载某个资源包中类型为type的所有资源

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| bundleName      | 资源包名称        |
| type      | 想要加载的资源类型        |