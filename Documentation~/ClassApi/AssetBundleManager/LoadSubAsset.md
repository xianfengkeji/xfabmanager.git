# AssetBundleManager.LoadSubAsset



### 方法:

public static T LoadSubAsset<*T*>(string projectName, string mainAssetName, string subAssetName)where T : UnityEngine.Object

### 说明:

加载子资源(泛型方式)

### 适用场景：

可用于加载 Sprite 下面的某一张子图片 ， 如下: 

![](texture/sub_assets.jpg)
 


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 资源类型        |
| projectName      | 资源模块名称        |
| mainAssetName      | 主资源名称        |
| subAssetName      | 子资源名称        |

### 返回值

类型 : **T**  

字段介绍 : 资源类型

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class TestLoadSubAsset : MonoBehaviour
>{
>    // Fix编码 
>    void Start()
>    {
>        // 加载 sprite 下面的 sprite_0 图片
>        Sprite sprite = AssetBundleManager.LoadSubAsset<Sprite>("Test", "sprite", "sprite_0");
>    }
>}
> ```


### 重载方法:
 
public static UnityEngine.Object LoadSubAsset(string projectName, string mainAssetName, string subAssetName, Type type);

### 说明:


加载子资源(非泛型方式)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| mainAssetName      | 主资源名称        |
| subAssetName      | 子资源名称        |
| type      | 资源类型        |

