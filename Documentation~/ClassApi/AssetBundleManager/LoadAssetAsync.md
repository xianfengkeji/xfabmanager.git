# AssetBundleManager.LoadAssetAsync



### 方法:

public static LoadAssetRequest LoadAssetAsync<*T*>(string projectName, string assetName);

### 说明:

异步加载资源(泛型方式)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 资源类型        |
| projectName      | 资源模块名称        |
| assetName      | 资源名称        |

### 返回值

类型 : **LoadAssetRequest**     **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| asset      | 加载到的资源对象    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAssetAsyncExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 异步加载资源
>        LoadAssetRequest request = AssetBundleManager.LoadAssetAsync<GameObject>("projectName","assetName");
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 资源加载成功
>            Debug.LogFormat("资源名称:{0}",request.asset.name);
>        }
>        else {
>            Debug.LogFormat("资源加载失败:{0}",request.error);
>        }
>    }
>}
> ```


### 重载方法:


public static LoadAssetRequest LoadAssetAsync(string projectName, string assetName, Type type);

### 说明:


异步加载资源(非泛型方式)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| assetName      | 资源名称        |
| type      | 资源类型        |