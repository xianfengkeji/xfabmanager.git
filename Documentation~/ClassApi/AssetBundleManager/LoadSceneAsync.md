# AssetBundleManager.LoadSceneAsync



### 方法:

public static AsyncOperation LoadSceneAsync(string projectName , string sceneName, LoadSceneMode mode);

### 说明:

异步加载打包在AssetBundle中的场景


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| sceneName      | 场景名称        |
| mode      | 场景加载模式(具体含义可查阅[Unity官方文档](https://docs.unity.cn/cn/2019.4/ScriptReference/SceneManagement.LoadSceneMode.html))        |


### 返回值

类型 : **AsyncOperation**    具体含义可查阅[Unity官方文档](https://docs.unity.cn/cn/2019.4/ScriptReference/AsyncOperation.html)



### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadSceneAsyncExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 加载场景
>        AsyncOperation operation = AssetBundleManager.LoadSceneAsync("projectName", "sceneName", UnityEngine.SceneManagement.LoadSceneMode.Single);
>        if (operation == null) {
>            Debug.LogFormat("场景加载失败,未查询到场景资源!");
>        }
>        yield return operation;
>        // 场景加载完成!
>    }
>}
> ```