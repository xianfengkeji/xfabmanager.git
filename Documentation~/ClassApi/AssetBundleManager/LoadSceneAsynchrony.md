# AssetBundleManager.LoadSceneAsynchrony



### 方法:

public static LoadSceneRequest LoadSceneAsynchrony(string projectName, string sceneName, LoadSceneMode mode);

### 说明:

异步加载打包在AssetBundle中的场景


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| sceneName      | 场景名称        |
| mode      | 场景加载模式(具体含义可查阅[Unity官方文档](https://docs.unity.cn/cn/2019.4/ScriptReference/SceneManagement.LoadSceneMode.html))        |


### 返回值
 
类型 : **LoadSceneRequest**     **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| Scene      | 加载到的场景    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadSceneAsyncExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 加载场景
>        LoadSceneRequest operation = AssetBundleManager.LoadSceneAsync("projectName", "sceneName", UnityEngine.SceneManagement.LoadSceneMode.Single);
>        if (operation == null) {
>            Debug.LogFormat("场景加载失败,未查询到场景资源!");
>        }
>        yield return operation;
>        // 场景加载完成!
>    }
>}
> ```