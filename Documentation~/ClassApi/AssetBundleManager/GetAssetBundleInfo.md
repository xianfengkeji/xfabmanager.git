# AssetBundleManager.GetAssetBundleInfo



### 方法:

public static string GetAssetBundleInfo();

### 说明:

获取当前已加载的资源信息(可根据此信息查看AssetBundle的释放情况)

**注:资源加载方式必须为AssetBundle,如果是从Assets加载,数据会一直为空**


### 返回值

类型 : **string** 

字段介绍 :  当前已加载的AssetBundle的信息

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetAssetBundleInfoExample : MonoBehaviour
>{
>    void Start()
>    {
>        string info = AssetBundleManager.GetAssetBundleInfo();
>        Debug.LogFormat("已加载的AssetBundle:{0}",info);
>    }
>}
> ```