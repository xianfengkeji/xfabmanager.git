# AssetBundleManager.IsLoadedAssetBundle



### 方法:

public static bool IsLoadedAssetBundle(string projectName, string bundleName);

### 说明:

判断是否已经加载某个AssetBundle

如果是编辑器模式并且从Assets加载资源会一直返回True


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| bundleName      | 资源包名称        |

### 返回值

类型 : **boolean** 

字段介绍 : 如果已加载某个AssetBundle则返回true，没加载则返回false

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsLoadedAssetBundleExample : MonoBehaviour
>{
>    void Start()
>    {
>        bool isLoaded = AssetBundleManager.IsLoadedAssetBundle("projectName","bundleName");
>        Debug.LogFormat("AssetBundle 是否已加载:{0}", isLoaded);
>    }
>}
> ```