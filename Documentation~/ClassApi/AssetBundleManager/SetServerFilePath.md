# AssetBundleManager.SetServerFilePath



### 方法:

public static void SetServerFilePath(IServerFilePath serverFilePath);

### 说明:

设置服务端文件路径接口

服务端的文件路径默认是按照如下规则进行拼接的:

url/projectName/version/Platform/fileName

假如各值如下:
| 名称        | 说明 |
| ----------- | ----------- |
| url      | http://127.0.0.1/AssetBundles       |
| projectName      | Test       |
| version      | 1.0.0      |
| Platform      | Android    |
| fileName      | test.unity3d   |

则 test.unity3d 的网络地址为: 

>```none
>http://127.0.0.1/AssetBundles/Test/1.0.0/Android/test.unity3d 
>```

假如你希望通过其他的规则来拼接地址可以实现接口IServerFilePath，然后创建该接口的对象, 通过方法AssetBundleManager.SetServerFilePath来设置!即可自定义服务端文件路径地址!


**注:一般情况下不推荐自定义,因为打包出来的资源路径是按照上方默认地址存放的,如果自定义的话，打包出来的资源不能直接放到服务器上，需要调整成自定义的路径规则，比较麻烦,除非有特殊的需求!**

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| serverFilePath      | IServerFilePath 的对象        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CustomServerFilePath : IServerFilePath
>{
>    public string ServerPath(string url, string projectName, string version, string fileName)
>    {
>        // 自定义你的文件路径 TODO 
>        return string.Empty;
>    }
>}
>public class SetServerFilePathExample : MonoBehaviour
>{ 
>    void Start()
>    {
>        CustomServerFilePath path = new CustomServerFilePath();
>        // 设置自定义的网络文件路径拼接方式
>        AssetBundleManager.SetServerFilePath(path);
>    }
>}
> ```
