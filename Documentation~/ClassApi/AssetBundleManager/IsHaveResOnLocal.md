# AssetBundleManager.IsHaveResOnLocal



### 方法:

public static bool IsHaveResOnLocal(string projectName);

### 说明:

判断本地是否有某个模块资源


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **boolean**    
字段介绍 : 

true : 有 false : 没有

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsHaveResOnLocalExample : MonoBehaviour
>{
>    void Start()
>    {
>        Debug.LogFormat("是否有资源:{0}", AssetBundleManager.IsHaveResOnLocal("projectName"));
>    }
>}
> ```
