# AssetBundleManager.UnLoadAllAssetBundles



### 方法:

public static void UnLoadAllAssetBundles(string projectName, bool unloadAllLoadedObjects = true);

### 说明:

卸载某个资源模块的所有AssetBundle


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| unloadAllLoadedObjects      | 是否卸载已加载的资源 默认:true        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UnLoadAllAssetBundlesExample : MonoBehaviour
>{
>    void Start()
>    {
>        AssetBundleManager.UnLoadAllAssetBundles("projectName", true);
>    }
>}
> ```