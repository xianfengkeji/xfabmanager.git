# AssetBundleManager.GetAssetBundleDependences



### 方法:

public static string[] GetAssetBundleDependences(string projectName, string bundleName);

### 说明:

获取AssetBundle的依赖


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| bundleName      | 资源包名称        |

### 返回值

类型 : **string[]**   

字段介绍 :  依赖的资源包名称

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetAssetBundleDependencesExample : MonoBehaviour
>{
>    void Start()
>    {
>        string[] bundles = AssetBundleManager.GetAssetBundleDependences("projectName","bundleName");
>        foreach (string bundle in bundles)
>        {
>            Debug.LogFormat("依赖的bundle:{0}",bundle);
>        }
>    }
>}
> ```