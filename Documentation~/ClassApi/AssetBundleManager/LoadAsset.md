# AssetBundleManager.LoadAsset



### 方法:

public static T LoadAsset<*T*>(string projectName , string assetName);

### 说明:

加载资源(泛型方式)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 资源类型        |
| projectName      | 资源模块名称        |
| assetName      | 资源名称        |

### 返回值

类型 : **T**  

字段介绍 : 资源类型

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAssetExample : MonoBehaviour
>{
>    void Start()
>    {
>        GameObject prefab = AssetBundleManager.LoadAsset<GameObject>("projectName","assetName");
>        Sprite sprite = AssetBundleManager.LoadAsset<Sprite>("projectName", "assetName");
>        AudioClip clip = AssetBundleManager.LoadAsset<AudioClip>("projectName", "assetName");
>        ...
>    }
>}
> ```


### 重载方法:


public static UnityEngine.Object LoadAsset(string projectName, string assetName, Type type);

### 说明:


加载资源(非泛型方式)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| assetName      | 资源名称        |
| type      | 资源类型        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAssetExample : MonoBehaviour
>{
>    void Start()
>    {
>        GameObject prefab = AssetBundleManager.LoadAsset("projectName","assetName",typeof(GameObject)) as GameObject;
>        Sprite sprite = AssetBundleManager.LoadAsset("projectName", "assetName",typeof(Sprite)) as Sprite;
>        AudioClip clip = AssetBundleManager.LoadAsset("projectName", "assetName",typeof(AudioClip)) as AudioClip;
>        ...
>    }
>}
> ```

