# AssetBundleManager.GetSecret



### 方法:

public static string GetSecret(string projectName);

### 说明:

获取某个模块的资源解密密钥


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **string** 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetSecretExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 设置密钥
>        string secret = AssetBundleManager.GetSecret("projectName");
>        Debug.LogFormat("密钥:{0}",secret);
>    }
>}
> ```