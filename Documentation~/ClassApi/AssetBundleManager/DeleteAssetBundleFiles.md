# AssetBundleManager.DeleteAssetBundleFiles


### 方法:

public static void DeleteAssetBundleFiles(string projectName);

### 说明:

删除某一个模块的所有AssetBundle文件,文件删除后资源将无法加载，若想继续使用请重新准备资源(释放或下载等)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DeleteAssetBundleFilesExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 删除某一个模块的所有AssetBundle文件
>        AssetBundleManager.DeleteAssetBundleFiles("projectName");
>    }
>}
> ```