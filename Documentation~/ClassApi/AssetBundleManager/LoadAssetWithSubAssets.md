# AssetBundleManager.LoadAssetWithSubAssets



### 方法:

public static T[] LoadAssetWithSubAssets<*T*>(string projectName , string assetName) where T : UnityEngine.Object;

### 说明:

加载资源及子资源(泛型方式)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 资源类型        |
| projectName      | 资源模块名称        |
| assetName      | 资源名称        |

### 返回值

类型 : **T[]** 

字段介绍 : 资源数组

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAssetWithSubAssetsExample : MonoBehaviour
>{
>    void Start()
>    {
>        Sprite[] sprites = AssetBundleManager.LoadAssetWithSubAssets<Sprite>("projectName", "assetName");
>        if (sprites != null) { 
>            foreach (Sprite sprite in sprites)
>            {
>                Debug.LogFormat("资源名称:{0}",sprite.name);
>            }
>        }
>    }
>}
> ```


### 重载方法:


public static UnityEngine.Object[] LoadAssetWithSubAssets(string projectName , string assetName, Type type);

### 说明:


加载资源及子资源(非泛型方式)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| assetName      | 资源名称        |
| type      | 资源类型        |
