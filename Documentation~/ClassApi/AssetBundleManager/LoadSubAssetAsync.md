# AssetBundleManager.LoadSubAssetAsync



### 方法:

public static LoadSubAssetRequest LoadSubAssetAsync<*T*>(string projectName, string mainAssetName, string subAssetName)where T : UnityEngine.Object

### 说明:

异步加载子资源(泛型方式)


### 适用场景：

可用于加载 Sprite 下面的某一张子图片 ， 如下: 

![](texture/sub_assets.jpg)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 资源类型        |
| projectName      | 资源模块名称        |
| mainAssetName      | 主资源名称        |
| subAssetName      | 子资源名称        |

### 返回值

类型 : **LoadSubAssetRequest**     **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| asset      | 加载到的资源对象    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAssetAsyncExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 加载 sprite 下面的 sprite_0 图片
>        LoadSubAssetRequest request_sub = AssetBundleManager.LoadSubAssetAsync<Sprite>("Test", "sprite", "sprite_0");
>        yield return request_sub;
>        if (string.IsNullOrEmpty(request_sub.error))
>        {
>            Debug.LogFormat("资源加载成功:{0}", request_sub.asset);
>        }
>        else {
>            Debug.LogFormat("资源加载失败:{0}", request_sub.error);
>        }
>    }
>}
> ```


### 重载方法:


public static LoadSubAssetRequest LoadSubAssetAsync(string projectName, string mainAssetName, string subAssetName, Type type);

### 说明:


异步加载子资源(非泛型方式)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| mainAssetName      | 主资源名称        |
| subAssetName      | 子资源名称        |
| type      | 资源类型        |