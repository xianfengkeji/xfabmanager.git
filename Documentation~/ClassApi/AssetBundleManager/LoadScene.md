# AssetBundleManager.LoadScene


### 方法:


public static void LoadScene(string projectName , string sceneName, LoadSceneMode mode);

### 说明:

加载打包在AssetBundle中的场景


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| sceneName      | 场景名称        |
| mode      | 场景加载模式(具体含义可查阅[Unity官方文档](https://docs.unity.cn/cn/2019.4/ScriptReference/SceneManagement.LoadSceneMode.html))        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadSceneExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 加载场景
>        AssetBundleManager.LoadScene("projectName", "sceneName", UnityEngine.SceneManagement.LoadSceneMode.Single);
>    }
>}
> ```