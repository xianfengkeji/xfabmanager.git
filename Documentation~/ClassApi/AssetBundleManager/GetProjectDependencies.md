# AssetBundleManager.GetProjectDependencies



### 方法:

public static GetProjectDependenciesRequest GetProjectDependencies(string projectName);

### 说明:

获取某个项目的依赖项目


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **GetProjectDependenciesRequest**     **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| dependencies      | 依赖的项目名称    |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetProjectDependenciesExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        GetProjectDependenciesRequest request = AssetBundleManager.GetProjectDependencies("projectName");
>        yield return request;
>        if (string.IsNullOrEmpty(request.error)) 
>        {
>            // 依赖项目获取成功
>            foreach (string dependency in request.dependencies) {
>                Debug.LogFormat("依赖:{0}",dependency);
>            }
>        }
>        else
>        {
>            Debug.LogFormat("依赖项目获取失败:{0}",request.error);
>        }
>    }
>}
> ```