# AssetBundleManager.SetGetProjectVersion



### 方法:

public static void SetGetProjectVersion<*T*>() where T : IGetProjectVersion;

### 说明:

自定义获取版本的接口(泛型方式)

在更新或下载资源之前, 首先需要获取到当前使用的资源的版本号, 然后去下载或者更新对应版本的资源，

默认的资源版本获取的方式为: 

读取 url/versions/projectName/version.txt 该路径下的文本的内容为资源版本号!

其中url为设置的项目的热更地址,projectName为资源模块名称, 

假如**url**为http://127.0.0.1/AssetBundles/，projectName为Test那版本文件的地址则为：

*http://127.0.0.1/AssetBundles/versions/Test/version.txt*

系统读取到该文本的内容,就会认为是Test模块的资源版本号!

在游戏开发中如果通过这种方式来管理资源版本太过简陋,一般情况下会通过后台来管理资源版本,更新等功能,这个时候可能就需要通过一个 Web Api 接口来获取资源的版本号,

如果是这种情况就需要自定义项目的版本获取方式!具体可参考下方代码实例!

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 实现了接口：**IGetProjectVersion** 的类型   |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CustomGetVersion : IGetProjectVersion
>{
>    // 该方法返回获取资源版本的过程中是否出现异常,如果成功获取到返回空即可
>    public string Error()
>    {
>        return string.Empty;
>    }
>
>    // 开始获取资源的版本号
>    public void GetProjectVersion(string projectName)
>    {
>        // 获取资源版本号 TODO 
>        Debug.LogFormat("开始获取资源版本号,资源模块名:{0}",projectName);
>    }
>
>    // 如果正在获取中返回false, 如果已经获取完成返回true(无论成功或失败)
>    public bool isDone()
>    {
>        return false;
>    }
>
>    // 返回获取到的版本 如果获取失败返回空即可
>    public string Result()
>    {
>        return "1.0.0";
>    }
>}
>
>
>public class SetGetProjectVersionExample : MonoBehaviour
>{ 
>    void Start()
>    {
>        // 设置获取版本号的方式
>        AssetBundleManager.SetGetProjectVersion<CustomGetVersion>();
>        // 此时获取版本号就会使用 CustomGetVersion 来获取
>    }
>}
> ```


### 重载方法:


public static void SetGetProjectVersion(Type type);

### 说明:


自定义获取版本的接口(非泛型方式)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| type     | 实现了接口：**IGetProjectVersion** 的类型   |
