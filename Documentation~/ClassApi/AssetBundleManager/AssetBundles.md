# AssetBundleManager.AssetBundles

public static Dictionary<string, Dictionary<string, AssetBundle>> AssetBundles;


### 说明:

该属性保存所有已加载的AssetBundle. 只读

key ： 类型 string  资源模块名称

value : 类型 Dictionary<string,AssetBundle>  key : AssetBundle名称 value: AssetBundle


### 用法如下:

> ```none
>
>using UnityEngine;
>
>public class AssetBundleExample : MonoBehaviour
>{
>
>    void Start()
>    {
>        foreach (var projectName in AssetBundleManager.AssetBundles.Keys)
>        { 
>            Dictionary<string,AssetBundle> bundles = AssetBundleManager.AssetBundles[projectName];
>            Debug.LogFormat("资源模块:{0} 已加载 AssetBundle 数量:{1}", projectName, bundles.Count);    
>            foreach (var bundleName in bundles.Keys)
>            {
>                Debug.LogFormat("资源包名:{0} AssetBundle:{1}", bundleName, bundles[bundleName].ToString());
>            } 
>        }
>    }
>
>}
> ```
