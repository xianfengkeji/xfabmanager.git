# AssetBundleManager.ExecuteOnlyOnceAtATime



### 方法:

public static T ExecuteOnlyOnceAtATime<*T*>(string key, AsyncOperationDelegate execute) where T : CustomYieldInstruction;

### 说明:

执行一个异步请求,如果该异步请求已经在执行中,则返回执行中的请求对象,避免重复执行同一请求,该异步请求必须继承CustomYieldInstruction


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 继承自CustomYieldInstruction的类型        |
| key      | 唯一标识        |
| execute      | 需要执行的委托        |


### 返回值

类型 : **T**     **[可挂起](ReadyRes.md#返回值)**


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UpdateOrDownloadResExample : MonoBehaviour
>{
>        internal static GetFileFromServerRequest GetFileFromServer(string projectName, string version, string fileName)
>        {
>            string key = string.Format("GetFileFromServer:{0}{1}{2}", projectName, version, fileName);
>            return ExecuteOnlyOnceAtATime<GetFileFromServerRequest>(key, () =>
>            {
>                GetFileFromServerRequest request = new GetFileFromServerRequest(GetProfile(projectName).url);
>                CoroutineStarter.Start(request.GetFileFromServer(projectName, version, fileName));
>                return request;
>            });
>        }
>}
> ```


 
