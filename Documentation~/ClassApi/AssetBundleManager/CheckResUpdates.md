# AssetBundleManager.CheckResUpdate



### 方法:

public static CheckResUpdateRequest CheckResUpdate(string projectName);

### 说明:

检测某个项目,是否需要更新、下载、或者 释放、 等


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **CheckResUpdateRequest**  **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| result      | 检测更新的结果 |

**CheckUpdateResult**  字段介绍
| 名称        | 说明 |
| ----------- | ----------- |
| updateType      | 需要执行的操作 |
| updateSize      | 需要更新的文件总大小 单位 字节        |
| message      | 更新内容，下图中填写的内容   ![](texture/message.png)    |
| projectName      | 资源模块名    |
| need_update_bundles      | 需要更新的资源包信息 包含 名称 大小 md5 |
| version      | 当前模块资源版本 |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CheckResUpdateExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 检测资源更新
>        CheckResUpdateRequest request = AssetBundleManager.CheckResUpdate("Test");
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 资源检测成功
>            Debug.LogFormat("检测结果:{0}",request.result);
>        }
>        else 
>        {
>            Debug.LogFormat("资源检测失败:{0}",request.error);    
>        }
>    }
>}
> ```


### 相似方法:


public static CheckResUpdatesRequest CheckResUpdates(string projectName)

### 说明:

检测某个项目及其依赖项目,是否需要更新、下载、或者 释放、 等

### 返回值

类型 : **CheckResUpdatesRequest**  **[可挂起](ReadyRes.md#返回值)**

该返回值与CheckResUpdateRequest基本相同, 只不过 CheckResUpdatesRequest.results 为数组,是一个或多个项目的检测结果!
