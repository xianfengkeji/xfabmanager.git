# AssetBundleManager.LoadAllAssetsAsync



### 方法:

public static LoadAssetsRequest LoadAllAssetsAsync(string projectName, string bundleName);

### 说明:

异步加载某个资源包中所有的资源


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| bundleName      | 资源包名称        |

### 返回值

类型 : **LoadAssetsRequest**     **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| assets      | 加载完成的资源数组    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAllAssetsAsyncExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 加载某个bundle中所有的资源
>        LoadAssetsRequest request = AssetBundleManager.LoadAllAssetsAsync("projectName", "bundleName");
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 资源加载成功
>            foreach (var asset in request.assets)
>            {
>                Debug.LogFormat("资源名称:{0}", asset.name);
>            }
>        }
>        else 
>        {
>            Debug.LogFormat("资源加载失败:{0}",request.error);
>        }
>    }
>}
> ```


### 重载方法:


public static LoadAssetsRequest LoadAllAssetsAsync<*T*>(string projectName, string bundleName) where T : UnityEngine.Object;


### 说明:


异步加载某个资源包中类型为T的所有资源

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 想要加载的资源类型        |
| projectName      | 资源模块名称        |
| bundleName      | 资源包名称        |


### 重载方法:


public static LoadAssetsRequest LoadAllAssetsAsync(string projectName, string bundleName, Type type);


### 说明:


异步加载某个资源包中类型为type的所有资源

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| bundleName      | 资源包名称        |
| type      | 想要加载的资源类型        |
