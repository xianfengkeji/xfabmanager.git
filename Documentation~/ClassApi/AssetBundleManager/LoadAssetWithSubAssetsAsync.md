# AssetBundleManager.LoadAssetWithSubAssetsAsync



### 方法:

public static LoadAssetsRequest LoadAssetWithSubAssetsAsync<*T*>(string projectName , string assetName) where T : UnityEngine.Object;

### 说明:

异步加载子资源(泛型方式)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| T      | 资源类型        |
| projectName      | 资源模块名称        |
| assetName      | 资源名称        |

### 返回值

类型 : **LoadAssetsRequest**     **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| assets      | 加载到的资源数组    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAssetWithSubAssetsAsyncExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        LoadAssetsRequest request = AssetBundleManager.LoadAssetWithSubAssetsAsync<Sprite>("projectName","assetName");
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            foreach (var item in request.assets)
>            {
>                Debug.LogFormat("资源名称:{0}",item.name);
>            }
>        }
>        else 
>        {
>            Debug.LogFormat("资源加载失败:{0}",request.error);
>        }
>    }
>}
> ```


### 重载方法:


public static LoadAssetsRequest LoadAssetWithSubAssetsAsync(string projectName, string assetName, Type type);

### 说明:


异步加载子资源(非泛型方式)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| assetName      | 资源名称        |
| type      | 资源类型        |
