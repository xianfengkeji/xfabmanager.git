# AssetBundleManager.SetSecret



### 方法:

public static void SetSecret(string projectName, string secret);

### 说明:

设置某一个模块的资源解密密钥 

**注:请务必保证密钥设置正确, 如果未加密请不要设置,否则会导致资源加载异常**


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| secret      | 加密的密钥        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class SetSecretExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 设置密钥
>        AssetBundleManager.SetSecret("projectName", "secret");
>    }
>}
> ```