# AssetBundleManager.SetProjectUpdateUrl



### 方法:

public static void SetProjectUpdateUrl(string url, string projectName = "");

### 说明:

手动设置某一个项目的更新地址 如果projectName为空,则对所有项目的有效,如果不为空仅对指定项目有效!


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| url      | 资源热更新地址       |
| projectName      | 资源模块名称,默认为空        |


**注:url需要填写的是资源模块所在的上一层目录，例如下图中MainModule资源上一层目录是AssetBundles,所以url应该填AssetBundles这个文件夹的网络路径，比如:http://127.0.0.1/AssetBundles/**

![](texture/url.png)

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class SetProjectUpdateUrlExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 设置所有资源模块的热更地址为 http://127.0.0.1/AssetBundles
>        AssetBundleManager.SetProjectUpdateUrl("http://127.0.0.1/AssetBundles");
>        // 设置Test1的热更地址为 http://127.0.0.1/AssetBundles
>        AssetBundleManager.SetProjectUpdateUrl("http://127.0.0.1/AssetBundles","Test1");
>    }
>}
> ```