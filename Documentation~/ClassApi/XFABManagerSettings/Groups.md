# XFABManagerSettings.Groups



### 字段:

public List<*string*> Groups;

### 说明:

所有的配置分组

如下图:

![](texture/groups.jpg)


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GroupsExample : MonoBehaviour
>{
>        void Start()
>        {
>            Debug.LogFormat("所有分组数量:{0}", XFABManagerSettings.Instances.Groups.Count);
>
>            foreach (var item in XFABManagerSettings.Instances.Groups)
>            {
>                Debug.LogFormat("分组名称 : {0}", item);
>            } 
>        }
>}
> ```
