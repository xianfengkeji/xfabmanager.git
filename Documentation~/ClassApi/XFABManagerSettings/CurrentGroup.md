# XFABManagerSettings.CurrentGroup



### 属性:

public string CurrentGroup;

### 说明:

当前选择的配置组的名称(只读)

下图中当前选中的分组为Test,则该字段返回值为Test!

![](texture/groups.jpg)

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CurrentGroupExample : MonoBehaviour
>{
>        void Start()
>        {
>            Debug.LogFormat("当前选择的配置组名称:{0}", XFABManagerSettings.Instances.CurrentGroup);
>        }
>}
> ```