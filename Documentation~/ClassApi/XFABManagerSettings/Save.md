# XFABManagerSettings.Save



### 方法:

public void Save();

### 说明:

 保存修改(仅在编辑器模式下有效)
 
### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class SaveExample : MonoBehaviour
>{
>        void Start()
>        {
>            // 以列表的形式显示项目列表
>            XFABManagerSettings.Instances.ShowMode = ProjectsShowMode.List;
>            // 保存修改
>            XFABManagerSettings.Instances.Save();
>        }
>}
> ```