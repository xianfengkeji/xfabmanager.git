# XFABManagerSettings.SetTargetGroup



### 方法:

public void SetTargetGroup(string group_name);

### 说明:

设置当前选择的配置组(仅在编辑器模式下有效)

下图中,当前选中的配置组为Test,如果想修改为Default或者Master，可通过该方法，传入对应的名称即可（也可传入下标，下标从0开始，Default:0 Test:1 Master:2）!

![](texture/groups.jpg)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| group_name      | 配置组名称        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class SetTargetGroupExample : MonoBehaviour
>{
>        void Start()
>        {
>            // 修改当前使用的配置组为Test
>            XFABManagerSettings.Instances.SetTargetGroup("Test");
>        }
>}
> ```


### 重载方法:


public void SetTargetGroup(int group_index);

### 说明:


根据下标设置当前选择的配置组(仅在编辑器模式下有效)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| group_index      | 配置组下标       |
