# XFABManagerSettings.CurrentProfiles



### 属性:

public Profile[] CurrentProfiles;

### 说明:

当前正在使用的配置组的所有配置选项(只读)

下图中当前选中的分组为Test,则该字段返回则为Test分组下的所有配置，即为Default和c！

![](texture/current_profiles.jpg)


### [Profile介绍](/Documentation~/ClassApi/AssetBundleManager/GetProfile.md#返回值) 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ProfilesExample : MonoBehaviour
>{
>        void Start()
>        {
>            Debug.LogFormat("当前配置:{0}", XFABManagerSettings.Instances.CurrentProfiles.Length);
>
>            foreach (var item in XFABManagerSettings.Instances.CurrentProfiles)
>            {
>                Debug.LogFormat("Profiles : {0}", JsonUtility.ToJson(item));
>            } 
>        }
>}
> ```