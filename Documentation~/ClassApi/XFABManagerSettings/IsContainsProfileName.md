# XFABManagerSettings.IsContainsProfileName



### 方法:

public bool IsContainsProfileName(string name,string groupName = "Default");

### 说明:

判断某一个配置组是否含有某一个配置选项


![](texture/profile.jpg)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| name      | 配置名称        |
| groupName      | 配置组名称        |

### 返回值

类型 : **boolean**  

字段介绍 : true：有 false:没有

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsContainsProfileNameExample : MonoBehaviour
>{
>        void Start()
>        {
>            // 判断Test分组中是否含有名称为profileName的配置
>            bool have = XFABManagerSettings.Instances.IsContainsProfileName("profileName", "Test");
>        }
>}
> ```