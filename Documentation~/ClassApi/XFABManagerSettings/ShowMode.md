# XFABManagerSettings.ShowMode



### 字段:

public ProjectsShowMode ShowMode;

### 说明:

格子的显示模式(仅在编辑器模式下有效)

可以通过修改该字段来控制XFABManager的界面是以格子的形式或者列表的形式来显示!

如下图:

![](texture/show_mode.jpg)

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ShowModeExample : MonoBehaviour
>{
>        void Start()
>        {
>            // 以格子的形式显示项目列表
>            XFABManagerSettings.Instances.ShowMode = ProjectsShowMode.Grid;
>            // 以列表的形式显示项目列表
>            XFABManagerSettings.Instances.ShowMode = ProjectsShowMode.List;
>            // 保存修改
>            XFABManagerSettings.Instances.Save();
>        }
>}
> ```