# XFABManagerSettings.SelectIndex



### 属性:

public int SelectIndex;

### 说明:

当前选择的配置组的下标（只读）

下图中当前选中的分组是Test,分组下标从0开始，Test是第二个分组，所以此时 SelectIndex的值为1！

![](texture/groups.jpg)

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class SelectIndexExample : MonoBehaviour
>{
>        void Start()
>        {
>            Debug.LogFormat("当前选择的配置下标:{0}", XFABManagerSettings.Instances.SelectIndex);
>        }
>}
> ```
