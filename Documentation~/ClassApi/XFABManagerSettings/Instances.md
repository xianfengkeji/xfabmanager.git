# XFABManagerSettings.Instances



### 属性:

public static XFABManagerSettings Instances;

### 说明:

XFABManagerSettings实例对象，该配置为单例，可通过该对象访问和修改配置!
 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class InstancesExample : MonoBehaviour
>{
>        void Start()
>        {
>            Debug.LogFormat("当前选择的配置组名称:{0}", XFABManagerSettings.Instances.CurrentGroup);
>        }
>}
> ```