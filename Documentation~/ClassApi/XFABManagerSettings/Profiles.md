# XFABManagerSettings.Profiles



### 字段:

public List<*Profile*> Profiles;

### 说明:

所有的配置列表

如下图:
![所有的配置列表](texture/all_profiles.jpg)

### [Profile介绍](/Documentation~/ClassApi/AssetBundleManager/GetProfile.md#返回值) 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ProfilesExample : MonoBehaviour
>{
>        void Start()
>        {
>            Debug.LogFormat("所有配置数量:{0}", XFABManagerSettings.Instances.Profiles.Count);
>
>            foreach (var item in XFABManagerSettings.Instances.Profiles)
>            {
>                Debug.LogFormat("Profiles : {0}", JsonUtility.ToJson(item));
>            } 
>        }
>}
> ```