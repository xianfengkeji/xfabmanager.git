# ProjectBuild
class in XFABManagerEditor

### 说明:

资源模块打包工具类



### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [Build](ProjectBuild/Build.md)      | 打包某一个模块的AssetBundle(仅可在Editor模式下调用)        |
| [CopyToStreamingAssets](ProjectBuild/CopyToStreamingAssets.md)      | 把打包完成的AssetBundle文件复制到StreamingAssets目录(仅可在Editor模式下调用)       |
| [CompressedIntoZip](ProjectBuild/CompressedIntoZip.md)      | 把打包完成的AssetBundle文件压缩成zip(仅可在Editor模式下调用)        |
| [PackAtlas](ProjectBuild/PackAtlas.md)      | 打包某一个资源模块的图集(仅可在Editor模式下调用)        |