# EditorApplicationTool
class in XFABManager

### 说明:

XFABManager工具类,提供一些公用的方法和功能!

### 静态属性

| 名称      | 说明 |
| ----------- | ----------- |
| isPlaying      | 判断当前编辑器是否处于运行状态(UnityEngine.Application.isPlaying在停止运行编辑器时触发OnDestroy时仍返回true)        | 