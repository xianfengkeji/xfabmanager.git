# DownloadFileRequest.AllSize



### 属性:

public long AllSize { get; private set; };

### 说明:

当前下载的文件总大小，单位:字节


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class AllSizeExample : MonoBehaviour
>{
>        IEnumerator Start()
>        {
>            DownloadFileRequest request = DownloadFileRequest.Download("url", "C:/test.txt");
>            while (!request.isDone)
>            {
>                yield return null;
>                // 文件总大小
>                Debug.LogFormat("文件总大小:{0} byte", request.AllSize);
>            }
>
>            if (string.IsNullOrEmpty(request.error))
>            {
>                // 文件下载成功
>            }
>            else {
>                // 文件下载失败
>                Debug.LogFormat("文件下载失败:{0}",request.error);
>            }
>        }
>}
> ```