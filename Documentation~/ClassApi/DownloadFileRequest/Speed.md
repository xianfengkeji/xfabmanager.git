# DownloadFileRequest.Speed



### 属性:

public long Speed { get; private set; };

### 说明:

下载速度，单位:字节/秒（只读）
 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class SpeedExample : MonoBehaviour
>{
>        IEnumerator Start()
>        {
>            DownloadFileRequest request = DownloadFileRequest.Download("url", "C:/test.txt");
>            while (!request.isDone)
>            {
>                yield return null;
>                // 当前下载速度
>                Debug.LogFormat("下载速度:{0} byte/s", request.Speed);
>            }
>
>            if (string.IsNullOrEmpty(request.error))
>            {
>                // 文件下载成功
>            }
>            else {
>                // 文件下载失败
>                Debug.LogFormat("文件下载失败:{0}",request.error);
>            }
>        }
>}
> ```
