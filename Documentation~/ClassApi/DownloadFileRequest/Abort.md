# DownloadFileRequest.Abort



### 方法:

public void Abort();

### 说明:

中断下载

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class AbortExample : MonoBehaviour
>{
>        void Start()
>        {
>            DownloadFileRequest request = DownloadFileRequest.Download("url", "C:/test.txt");
>
>            // 中断下载
>            request.Abort();
>        }
>}
> ```