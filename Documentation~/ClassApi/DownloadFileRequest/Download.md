# DownloadFileRequest.Download



### 方法:

public static DownloadFileRequest Download(string file_url, string localfile);

### 说明:

下载文件


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| file_url      | 需要下载的文件的网络地址        |
| localfile      | 下载之后存放在本地的路径        |

### 返回值

类型 : **DownloadFileRequest**     **[可挂起](/Documentation~/ClassApi/AssetBundleManager/ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
|  [Speed](Speed.md)  | 下载速度，单位:字节/秒       |
|  [DownloadedSize](DownloadedSize.md)  | 已经下载的文件大小，单位:字节      |
|  [AllSize](AllSize.md)  | 当前下载的文件总大小，单位:字节       |
|  [Url](Url.md)  | 当前正在下载的文件地址       |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DownloadExample : MonoBehaviour
>{
>       IEnumerator Start()
>       {
>           DownloadFileRequest request = DownloadFileRequest.Download("url", "C:/test.txt");
>            
>           // 下载的文件的地址
>           Debug.LogFormat("url:{0}",request.Url);>
>           while (!request.isDone)
>           {
>               yield return null; 
>           }>
>           if (string.IsNullOrEmpty(request.error))
>           {
>               // 文件下载成功
>           }
>           else {
>               // 文件下载失败
>               Debug.LogFormat("文件下载失败:{0}",request.error);
>           }
>       }
>}
> ```