# DownloadFileRequest.DownloadedSize



### 属性:

public long DownloadedSize { get; private set; };

### 说明:

已经下载的文件大小，单位:字节(只读)


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DownloadedSizeExample : MonoBehaviour
>{
>        IEnumerator Start()
>        {
>            DownloadFileRequest request = DownloadFileRequest.Download("url", "C:/test.txt");
>            while (!request.isDone)
>            {
>                yield return null;
>                // 已下载的大小
>                Debug.LogFormat("已下载的大小:{0} byte", request.DownloadedSize);
>            }
>
>            if (string.IsNullOrEmpty(request.error))
>            {
>                // 文件下载成功
>            }
>            else {
>                // 文件下载失败
>                Debug.LogFormat("文件下载失败:{0}",request.error);
>            }
>        }
>}
> ```