# DownloadFilesRequest
class in XFABManager

### 说明:

下载多个文件的请求类



### 事件

| 名称      | 说明 |
| ----------- | ----------- |
|  [onFileDownloadCompleted](DownloadFilesRequest/onFileDownloadCompleted.md)  | 当某个文件下载完成时触发       |

### 属性

| 名称      | 说明 |
| ----------- | ----------- |
|  [Speed](DownloadFilesRequest/Speed.md)  | 下载速度，单位:字节/秒       |
|  [DownloadedSize](DownloadFilesRequest/DownloadedSize.md)  | 已经下载的文件大小，单位:字节      |
|  [AllSize](DownloadFilesRequest/AllSize.md)  | 所有需要下载的文件总大小，单位:字节       |

### 方法

| 名称      | 说明 |
| ----------- | ----------- |
| [Abort](DownloadFilesRequest/Abort.md)      | 中断下载        |

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [DownloadFiles](DownloadFilesRequest/DownloadFiles.md)      | 下载一组文件        |