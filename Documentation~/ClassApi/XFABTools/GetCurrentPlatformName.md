# XFABTools.GetCurrentPlatformName



### 方法:

public static string GetCurrentPlatformName();

### 说明:

获取当前平台的名称


### 返回值

类型 : **string**   


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetCurrentPlatformNameExample : MonoBehaviour
>{
>    void Start()
>    {
>        string name = XFABTools.GetCurrentPlatformName();
>        Debug.LogFormat("当前平台:{0}",name);
>    }
>}
> ```