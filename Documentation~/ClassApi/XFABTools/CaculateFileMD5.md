# XFABTools.CaculateFileMD5



### 方法:

public static FileMD5Request CaculateFileMD5(string path);

### 说明:

异步计算md5的值,如果文件过大推荐使用此方法


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| path      | 需要就算md5值的文件路径        |

### 返回值

类型 : **FileMD5Request**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| md5      | 文件的md5值    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CaculateFileMD5Example : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        string file = "C:/test.txt";
>        FileMD5Request request = XFABTools.CaculateFileMD5(file);
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // md5计算成功
>            Debug.LogFormat("file:{0} md5:{1}",file,request.md5);
>        }
>        else
>        {
>            Debug.LogFormat("md5计算失败:{0}",request.error);    
>        }
>    }
>}
> ```