# XFABTools.DataPath



### 方法:

public static string DataPath(string projectName);

### 说明:

某个资源模块的数据存放目录


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **string** 


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DataPathExample : MonoBehaviour
>{
>    void Start()
>    {
>        string dir = XFABTools.DataPath("projectName");
>        Debug.LogFormat("数据存放目录:{0}",dir);
>    }
>}
> ```