# XFABTools.GetLocalVersion



### 方法:

public static string GetLocalVersion(string projectName);

### 说明:

查询本地某个模块的资源版本号


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        | 

### 返回值

类型 : **string**


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LocalResPathExample : MonoBehaviour
>{
>    void Start()
>    {
>        string version = XFABTools.GetLocalVersion("projectName");
>        Debug.LogFormat("缓存到本地的资源版本:{0}",version);
>    }
>}
> ```