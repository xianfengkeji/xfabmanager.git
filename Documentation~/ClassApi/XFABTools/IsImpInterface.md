# XFABTools.IsImpInterface



### 方法:

public static bool IsImpInterface(Type source,Type target);

### 说明:

判断一个类是否实现了某个接口


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| source      | 类型        |
| target      | 接口        |

### 返回值

类型 : **boolean**   

字段介绍 : true:实现了 false:没实现

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsImpInterfaceExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 判断Image是否实现了接口ICanvasRaycastFilter
>        bool IsImp = XFABTools.IsImpInterface(typeof(Image),typeof(ICanvasRaycastFilter));
>        Debug.LogFormat("IsImp:{0}", IsImp);
>    }
>}
> ```