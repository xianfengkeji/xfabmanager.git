# XFABTools.md5file



### 方法:

public static string md5file(string file);

### 说明:

计算文件的MD5值


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| file      | 需要计算md5的文件地址        |

### 返回值

类型 : **string**  

字段介绍 :  md5


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UpdateOrDownloadResExample : MonoBehaviour
>{
>    void Start()
>    {
>        string file = "C:/test.txt";
>        string md5 = XFABTools.md5file(file);
>        Debug.LogFormat("file:{0} md5:{1}",file, md5);
>    }
>}
> ```