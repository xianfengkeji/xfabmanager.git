# XFABTools.BuildInDataPath



### 方法:

public static string BuildInDataPath(string projectName);

### 说明:

内置数据目录(安装包中的数据的目录) 


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **string** 


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class BuildInDataPathExample : MonoBehaviour
>{
>    void Start()
>    {
>        string dir = XFABTools.BuildInDataPath("projectName");
>        Debug.LogFormat("内置数据目录:{0}",dir);
>    }
>}
> ```

### 重载方法:

public static string BuildInDataPath(string projectName, string fileName);

### 说明:

内置数据目录(安装包中的数据的目录) 

具体某个文件的路径

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| fileName      | 文件名       |