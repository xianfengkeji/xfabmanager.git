# XFABTools.LocalResPath



### 方法:

public static string LocalResPath(string projectName, string fileName);

### 说明:

缓存到本地的资源文件路径(数据目录的资源,加载资源时从该目录读取AssetBundle文件)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |
| fileName      | 文件名        |

### 返回值

类型 : **string**


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LocalResPathExample : MonoBehaviour
>{
>    void Start()
>    {
>        string dir = XFABTools.LocalResPath("projectName","fileName");
>        Debug.LogFormat("缓存到本地的资源文件路径:{0}",dir);
>    }
>}
> ```