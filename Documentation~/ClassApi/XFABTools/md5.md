# XFABTools.md5



### 方法:

public static string md5(string source);

### 说明:

计算字符串的MD5值


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| source      | 需要计算md5的字符串内容        |

### 返回值

类型 : **string** 

字段介绍 : md5

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class md5Example : MonoBehaviour
>{
>    void Start()
>    {
>        string md5 = XFABTools.md5("string");
>        Debug.LogFormat("md5:{0}", md5);
>    }
>}
> ```