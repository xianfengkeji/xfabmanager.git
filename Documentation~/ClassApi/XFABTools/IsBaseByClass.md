# XFABTools.IsBaseByClass



### 方法:

public static bool IsBaseByClass(Type source, Type target);

### 说明:

判断一个类是否继承另外一个类(如果传入两个相同的类会返回false)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| source      | 类型，子类        |
| target      | 类型，父类        |

### 返回值

类型 : **boolean** 

字段介绍 : true:继承 false:不继承

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsBaseByClassExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 判断GameObject是否继承自Object
>        bool isBase = XFABTools.IsBaseByClass(typeof(GameObject),typeof(Object));
>        Debug.LogFormat("isBase:{0}",isBase);
>    }
>}
> ```