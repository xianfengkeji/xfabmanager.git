# RemoteFileInfoRequest.RequestInfo



### 方法:

public static RemoteFileInfoRequest RequestInfo(string file_url);

### 说明:

请求网络文件信息


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| file_url      | 网络文件链接        |

### 返回值

类型 : **RemoteFileInfoRequest**     **[可挂起](/Documentation~/ClassApi/AssetBundleManager/ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| ExistType      | 网络文件存在类型（Exist:存在,UnExist:不存在,Unknown:未知）    |
| Length      | 网络文件的大小（单位:字节）    |
| Url      | 网络文件路径    |
| Type      | 网络文件类型    |
| LastModified      | 最后一次的修改时间    |
| AllInfo      | 当前网络文件的所有信息    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class RemoteFileInfoRequestExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        string url = "http://127.0.0.1/test.txt";
>        RemoteFileInfoRequest request = RemoteFileInfoRequest.RequestInfo(url);
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 网络信息请求成功 
>            Debug.LogFormat("网络文件存在类型:{0}", request.ExistType); 
>            Debug.LogFormat("网络文件的大小:{0}", request.Length);
>            Debug.LogFormat("网络文件路径:{0}", request.Url);
>            Debug.LogFormat("网络文件类型:{0}", request.Type);
>            Debug.LogFormat("最后一次的修改时间:{0}", request.LastModified); 
>
>            // 所有信息
>            foreach (var item in request.AllInfo.Keys)
>            {
>                Debug.LogFormat("key:{0} value:{1}", item, request.AllInfo[item]);
>            } 
>        }
>        else 
>        {
>            Debug.LogFormat("请问文件:{0}信息失败，error:{1}",url,request.error);
>        }
>    }
>}
> ```