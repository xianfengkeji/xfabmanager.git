# IPreprocessBuildProject	
class in XFABManagerEditor

### 说明:

某一个资源模块打包前的事件监听(仅在Editor模式下可用)


### 示例:

1.创建脚本并继承该接口（该脚本要放在Editor文件夹下）

> ```none
>
>using UnityEngine;
>
>public class TestLinstenner : IPreprocessBuildProject
>{
>    public void OnPreprocess(string projectName, string outputPath, BuildTarget buildTarget)
>    {
>        Debug.LogFormat("将要打包资源:{0} 资源路径:{1} 目标平台:{2}",projectName,outputPath, buildTarget);
>    }
>}
> ```

2.打包资源，在资源打包前时会触发上方的回调

![](/Documentation~/ClassApi/ProjectBuild/textures/build.jpeg)