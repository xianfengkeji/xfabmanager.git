# XFABManagerSettings
class in XFABManager

### 说明:

通过该类可以获取或修改插件配置,比如:显示模式，配置组等

具体如下图:

![](XFABManagerSettings/texture/profile.jpg)

### 字段

| 名称      | 说明 |
| ----------- | ----------- |
|  [Profiles](XFABManagerSettings/Profiles.md)  | 所有的配置列表       |
|  [Groups](XFABManagerSettings/Groups.md)  | 所有的配置分组       |
|  [ShowMode](XFABManagerSettings/ShowMode.md)  | 格子的显示模式(仅在编辑器模式下有效)       |

### 属性

| 名称      | 说明 |
| ----------- | ----------- |
|  [SelectIndex](XFABManagerSettings/SelectIndex.md)  | 当前选择的配置组的下标（只读）       |
|  [CurrentProfiles](XFABManagerSettings/CurrentProfiles.md)  | 当前正在使用的配置组的所有配置选项       |
|  [CurrentGroup](XFABManagerSettings/CurrentGroup.md)  | 当前选择的配置组的名称       |

### 静态属性

| 名称      | 说明 |
| ----------- | ----------- |
|  [Instances](XFABManagerSettings/Instances.md)  | XFABManagerSettings的实例       | 


### 方法

| 名称      | 说明 |
| ----------- | ----------- |
| [IsContainsProfileName](XFABManagerSettings/IsContainsProfileName.md)      | 判断某一个配置组是否含有某一个配置选项        |
| [SetTargetGroup](XFABManagerSettings/SetTargetGroup.md)      | 设置当前选择的配置组(仅在编辑器模式下有效)        |
| [Save](XFABManagerSettings/Save.md)      | 保存修改(仅在编辑器模式下有效)        | 