# FileTools
class in XFABManager

### 说明:

文件工具类

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [CopyDirectory](FileTools/CopyDirectory.md)      | 复制一个文件夹(可在编辑器非运行状态下调用)       |
| [Copy](FileTools/Copy.md)      | 异步复制一个文件(可用于Android平台下的StreamingAssets目录文件复制,不可在编辑器非运行状态下调用)         |
| [GetDirectorySize](FileTools/GetDirectorySize.md)      | 获取文件夹大小(可在编辑器非运行状态下调用)         |