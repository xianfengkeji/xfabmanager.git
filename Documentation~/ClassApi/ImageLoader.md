# ImageLoader
class in XFABManager

### 说明:

图片加载器

### 适用场景:

可用于UnityEngine.UI.Image,UnityEngine.UI.RawImage,UnityEngine.SpriteRenderer等组件的图片加载并赋值!

该组件可从网络，AssetBundle，本地文件，三种方式加载图片，并且通过该组件加载的图片会进行引用计数，
当图片不再使用时会主动释放。(推荐使用)

### 注意事项

假如通过ImageLoader加载AssetBundle中的某一个图片，那么则不建议再通过其他的方式使用该图片，因为ImageLoader会主动释放图片资源，
此时通过其它方式加载的图片有可能会被卸载掉，导致显示异常!!! 具体如下:


> ```none
>
>using UnityEngine;
>
>public class Example : MonoBehaviour
>{
>        void Start()
>        {
>            //第一种方式 通过ImageLoader加载Test模块中名称为headimage的图片
>            ImageLoader loader = GetComponent<ImageLoader>();
>            loader.Type = ImageLoaderType.AssetBundle;
>            loader.ProjectName = "Test";
>            loader.AssetName = "headimage";
>
>            //第二种方式 通过AssetBundleManager加载Test模块中名称为headimage的图片（不推荐使用）
>            // 因为headimage已经通过ImageLoader加载了，那么ImageLoader就会在适当的时候对该
>            // 图片资源进行卸载，此时通过AssetBundleManager加载的图片将无法正常使用!
>            Sprite sprite = AssetBundleManager.LoadAsset<Sprite>("Test","headimage");
>            UnityEngine.UI.Image image = GetComponent<UnityEngine.UI.Image>();
>            image.sprite = sprite; 
>        }
>}
> ```

**问:假如仅仅只通过ImageLoader一种方式加载某一张图片,但是该图片在某个预制体中有直接引用，这个时候会有问题吗?**

**答:不会有问题。因为这个图片被其他游戏物体引用了，那么该图片所在的AssetBundle就会被其他的AssetBundle依赖，当ImageLoader尝试卸载资源时，会检测AssetBundle是不是被其他的AssetBundle依赖，如果是的则不会卸载!**

### 使用方法:

**通过ImageLoader给UnityEngine.UI.Image组件加载图片（其他组件使用方法类似）**
1. 添加ImageLoader组件

![](ImageLoader/texture/image_loader_guide_1.jpg)
![](ImageLoader/texture/image_loader_guide_2.jpeg)

2. 设置加载图片的类型，也就是加载图片的方式，目前支持三种方式:

    1.Network加载网络图片

    2.Local加载本地图片

    3.AssetBundle从AssetBundle中加载图片

    这里我们演示从AssetBundle中加载图片，如果是从网络或者本地加载，只需要填写对应的网络文件或者本地文件路径即可

![](ImageLoader/texture/image_loader_guide_3_type.jpeg)

4. ProjectName 资源模块名 AssetName 资源名，如下图：

![](ImageLoader/texture/image_loader_guide_4_projectName_assetName.jpg)
![](ImageLoader/texture/image_loader_guide_5_projectName_assetName_des.jpeg)

5. TargetComponentType目标组件类型，也就是希望把加载到的图片赋值给哪个组件(必须与ImageLoader在同一个游戏物体上)

    目前默认支持三种组件，分别为UnityEngine.UI.Image,UnityEngine.UI.RawImage,UnityEngine.SpriteRenderer,如果您想在其他的组件上使用该功能，
    可以选择Other,填写对应的Target_component_full_name(目标组件名称，含命名空间)和Target_component_fields_name（目标组件的字段，例如：设置Image的图片需要对sprite字段赋值，那么该值即为sprite）

![](ImageLoader/texture/image_loader_guide_6.jpeg)

6. loading_color 加载中的颜色 loading_complete_color 加载完成时的颜色

![](ImageLoader/texture/image_loader_guide_7.jpeg)

![](ImageLoader/texture/image_loader_guide_8.jpeg)

![](ImageLoader/texture/image_loader_guide_9.jpeg) 


7. 使用代码控制图片加载

> ```none
>
>using UnityEngine;
>
>public class Example : MonoBehaviour
>{
>    void Start()
>    { 
>        ImageLoader loader = GetComponent<ImageLoader>();
>        // 加载AssetBundle
>        loader.Type = ImageLoaderType.AssetBundle;
>        loader.ProjectName = "Test";
>        loader.AssetName = "headimage";
>        // 加载并显示C盘下的test.png图片
>        loader.Type = ImageLoaderType.Local;
>        loader.Path = "c:/test.png";
>        // 加载网络图片
>        loader.Type = ImageLoaderType.Network;
>        loader.Path = "https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png"; 
>    }
>}
> ```

8. 加载Sprite中的某一个子图片 

例如加载下图中的图片: 

![](AssetBundleManager/texture/sub_assets.jpg)

设置如下: 

![](ImageLoader/texture/load_sub_asset.jpg)


### 属性

| 名称      | 说明 |
| ----------- | ----------- |
| Type       | 图片加载方式       |
| Path      | 图片加载路径 *注: 当加载图片的方式为 ImageLoaderType.Network 或 ImageLoaderType.Local 时有效       |
| ProjectName       | 待加载的图片资源所在的模块名 *注: 当加载图片的方式为 ImageLoaderType.AssetBundle 时有效       |
| AssetName        | 待加载的图片资源名 *注: 当加载图片的方式为 ImageLoaderType.AssetBundle 时有效,若加载Sprite下面的子图片,填写方式为:Sprite/Child      |


### 字段

| 名称      | 说明 |
| ----------- | ----------- |
| targetComponentType      | 加载到的图片赋值给目标组件的类型    |
| target_component_full_name      | 需要赋值的目标组件的全名(包含命名空间) 例如: UnityEngine.UI.Image       |
| target_component_fields_name     | 需要赋值的目标组件的字段名 例如: sprite    |
| loading_color      | 加载中的颜色    |
| load_complete_color       | 加载完成的颜色    |
| auto_set_native_size     | 是否自动 SetNativeSize    |

### 事件

| 名称      | 说明 |
| ----------- | ----------- |
| OnStartLoad      | 开始加载图片的回调      |
| OnLoading       | 正在加载的回调 参数: float: 加载进度(0-1)    |
| OnLoadCompleted       | 加载完成的回调 参数: bool:是否成功 ImageData:图像数据(如果没有加载成功 为空) string:如果加载失败,则是失败的原因,如果成功则为:success    |
| OnLoadSuccess      | 加载成功的回调(如果加载失败则不会触发)    |
| OnLoadFailure       | 加载失败的回调(如果加载成功则不会触发) 参数:string:失败的原因    |


### 方法

| 名称      | 说明 |
| ----------- | ----------- |
| Refresh      | 刷新 (如果加载图片失败,可调用此方法重新加载)      |
| Clear       | 清空当前显示的图片    | 