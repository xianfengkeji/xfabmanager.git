# ZipTools
class in XFABManager

### 说明:

压缩和解压的工具类

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [CreateZipFile](ZipTools/CreateZipFile.md)      | 压缩文件        |
| [UnZipFile](ZipTools/UnZipFile.md)      | 解压zip文件         |
| [UnZipFileAsync](ZipTools/UnZipFileAsync.md)      | 异步解压zip文件         |