# DownloadFileRequest
class in XFABManager

### 说明:

下载文件的请求类


### 属性

| 名称      | 说明 |
| ----------- | ----------- |
|  [Speed](DownloadFileRequest/Speed.md)  | 下载速度，单位:字节/秒       |
|  [DownloadedSize](DownloadFileRequest/DownloadedSize.md)  | 已经下载的文件大小，单位:字节      |
|  [AllSize](DownloadFileRequest/AllSize.md)  | 当前下载的文件总大小，单位:字节       |
|  [Url](DownloadFileRequest/Url.md)  | 当前正在下载的文件地址       |

### 方法

| 名称      | 说明 |
| ----------- | ----------- |
| [Abort](DownloadFileRequest/Abort.md)      | 中断下载        |

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [Download](DownloadFileRequest/Download.md)      | 下载文件        |