# GameObjectLoader.Load



### 方法:

public static GameObject Load(string projectName, string assetName, Transform parent = null);

### 说明:

加载预制体并进行实例化

*注:不再使用时可以直接销毁或者通过[Unload](UnLoad.md)回收!通过该方法创建的游戏物体会进行引用计数,当引用数量为0时,会主动卸载资源(推荐使用)*

*注:加载的过程中会首先去缓存池中添加查询有没有空闲的游戏物体，如果有则直接使用，如果没有再创建!*

```diff
- 请认真阅读下方的[代码示例]中的注释
```


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 预制体资源模块名称        |
| assetName      | 预制体资源的名称        |
| parent      | 创建出来的实例的父物体，默认为空        |

### 返回值

类型 : **GameObject**     **[可参考官方文档](https://docs.unity3d.com/Manual/class-GameObject.html)**
 
### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadExample : MonoBehaviour
>{
>   public Transform parent;
>   void Start()
>   {
>       // 实例化名为Cube的预制体
>       GameObject obj = GameObjectLoader.Load("Test", "Cube", parent);
>
>       // 不推荐以下写法
>       GameObject prefab = AssetBundleManager.LoadAsset<GameObject>("Test", "Cube");
>       GameObject obj1 = GameObject.Instantiate(prefab); 
>
>       *注：以上两种写法切勿同时使用，因为当通过GameObjectLoader.Load() 创建的游戏物体被全部销毁或者回收后，
>       // 会自动卸载Cube的预制体，此时通过GameObject.Instantiate(prefab)创建出来的游戏对象将无法正常使用!!!
>   }
>}
> ```




### 重载方法:

public static GameObject Load(GameObject prefab, Transform parent = null);

### 说明:

预制体实例化

该重载方法适用于不是通过AssetBundle加载的预制体的实例化（例如:Resource.Load,Inspector赋值，场景中的查找（GameObject.Find()）等）

假如预制体是通过AssetBundle加载的，则不建议使用该方法，如下：

> ```none
>        // 不推荐
>        GameObject prefab = AssetBundleManager.LoadAsset<GameObject>("Test", "Cube");
>        GameObject obj = GameObjectLoader.Load(prefab, parent); 
>
>        // 推荐
>        GameObject obj = GameObjectLoader.Load("Test", "Cube", parent);
> ```

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| prefab      | 预制体资源        |
| parent      | 创建出来的实例的父物体，默认为为空        |


### 返回值

类型 : **GameObject**     **[可参考官方文档](https://docs.unity3d.com/Manual/class-GameObject.html)**

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadExample : MonoBehaviour
>{
>    public Transform parent;
>    void Start()
>    { 
>        // 该重载方法适用于非AssetBundle加载的预制体的实例化（例如:Resource.Load,Inspector赋值，场景中的查找（GameObject.Find()）等）
>        GameObject prefab = Resources.Load<GameObject>("cube"); 
>        // 实例化名为Cube的预制体
>        GameObject obj = GameObjectLoader.Load(prefab, parent); 
>    }
>}
> ```
