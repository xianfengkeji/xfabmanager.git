# GameObjectLoader.LoadAsync



### 方法:

public static GameObjectLoadRequest LoadAsync(string projectName,string assetName, Transform parent = null);

### 说明:

异步加载预制体并进行实例化

*注:不再使用时可以直接销毁或者通过[Unload](UnLoad.md)回收!通过该方法创建的游戏物体会进行引用计数,当引用数量为0时,会主动卸载资源(推荐使用)*

*注:加载的过程中会首先去缓存池中添加查询有没有空闲的游戏物体，如果有则直接使用，如果没有再创建!*

```diff
- 请认真阅读下方的[代码示例]中的注释
```


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 预制体资源模块名称        |
| assetName      | 预制体资源的名称        |
| parent      | 创建出来的实例的父物体，默认为空        |

### 返回值

类型 : **GameObjectLoadRequest** 

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| Obj      | 实例化出来的游戏物体    |
 
### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class LoadAsyncExample : MonoBehaviour
>{
>   public Transform parent;
>    IEnumerator Start()
>    {
>        // 异步加载并实例化名为Cube的预制体
>        GameObjectLoadRequest request = GameObjectLoader.LoadAsync("Test", "Cube") ;
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 加载成功
>            GameObject obj = request.Obj;
>        }
>        else {
>            Debug.LogFormat("加载失败:{0}",request.error);
>        }
>
>        // 不推荐以下写法
>        LoadAssetRequest request_prefab = AssetBundleManager.LoadAssetAsync<GameObject>("Test", "Cube");
>        yield return request_prefab;
>        GameObject obj1 = GameObject.Instantiate(request_prefab.asset as GameObject);
>
>        // *注：以上两种写法切勿同时使用，因为当通过GameObjectLoader.LoadAsync() 创建的游戏物体被全部销毁或者回收后，
>        // 会自动卸载Cube的预制体，此时通过GameObject.Instantiate(prefab)创建出来的游戏对象将无法正常使用!!!
>    }
>}
> ```

