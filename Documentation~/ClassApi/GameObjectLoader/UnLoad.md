# GameObjectLoader.UnLoad



### 方法:

public static void UnLoad(GameObject obj);

### 说明:

回收或销毁某一个游戏物体，如果这个游戏物体是通过GameObjectLoader加载的, 则会放到缓存池中回收, 如果不是则会直接销毁
被回收的游戏物体如果超过五分钟没有使用会被销毁


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| obj      | 被回收的游戏物体对象        |
| parentStays      | 是否保持父节点不变,默认:false        |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UnLoadExample : MonoBehaviour
>{
>    void Start()
>    {  
>        // 实例化名为Cube的预制体
>        GameObject obj = GameObjectLoader.Load("Test","Cube", parent);
>        // 回收游戏物体
>        GameObjectLoader.UnLoad(obj); 
>        // *注:被回收的游戏物体会放到缓存池中等待下次使用，如果长时间没有使用才会被销毁(推荐使用)
>        // *注:不建议使用GameObject.Destroy销毁
>    }
>}
> ```