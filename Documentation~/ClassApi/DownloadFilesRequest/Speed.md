# DownloadFilesRequest.Speed



### 属性:

public long Speed { get; private set; };

### 说明:

下载速度，单位:字节/秒（只读）
 

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class SpeedExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 需要下载的文件集合
>        List<DownloadObjectInfo> needDownloadFiles = new List<DownloadObjectInfo>();
>
>        string url = "http://127.0.0.1/test.txt";   // 需要下载的文件的网络路径
>        string localfile = "C:/test.txt";           // 保存在本地的路径
>        DownloadObjectInfo download1 = new DownloadObjectInfo(url, localfile);
>        needDownloadFiles.Add(download1);
>
>        DownloadFilesRequest request = DownloadFilesRequest.DownloadFiles(needDownloadFiles);
>        while (!request.isDone)
>        {
>            yield return null;
>            Debug.LogFormat("下载速度:{0} byte/s",request.Speed);
>        }
>    }
>}
> ```
