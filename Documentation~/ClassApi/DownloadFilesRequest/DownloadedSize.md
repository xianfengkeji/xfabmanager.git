# DownloadFilesRequest.DownloadedSize



### 属性:

public long DownloadedSize;

### 说明:

已经下载的文件大小，单位:字节(只读)


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DownloadedSizeExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 需要下载的文件集合
>        List<DownloadObjectInfo> needDownloadFiles = new List<DownloadObjectInfo>();
>
>        string url = "http://127.0.0.1/test.txt";   // 需要下载的文件的网络路径
>        string localfile = "C:/test.txt";           // 保存在本地的路径
>        DownloadObjectInfo download1 = new DownloadObjectInfo(url, localfile);
>        needDownloadFiles.Add(download1);
>
>        DownloadFilesRequest request = DownloadFilesRequest.DownloadFiles(needDownloadFiles);
>        while (!request.isDone)
>        {
>            yield return null;
>            Debug.LogFormat("已下载文件大小:{0} byte",request.DownloadedSize);
>        }
>    }
>}
> ```