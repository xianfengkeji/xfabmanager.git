# DownloadFilesRequest.DownloadFiles



### 方法:

public static DownloadFilesRequest **DownloadFiles**(List<*DownloadObjectInfo*> files);

### 说明:

下载一组文件


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| files      | 需要下载的文件信息的集合        | 

### 返回值

类型 : **DownloadFilesRequest**     **[可挂起](/Documentation~/ClassApi/AssetBundleManager/ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
|  [Speed](Speed.md)  | 下载速度，单位:字节/秒       |
|  [DownloadedSize](DownloadedSize.md)  | 已经下载的文件大小，单位:字节      |
|  [AllSize](AllSize.md)  | 当前下载的文件总大小，单位:字节       |
|  [onFileDownloadCompleted](onFileDownloadCompleted.md)  | 当某个文件下载完成时触发       |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DownloadFilesExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 需要下载的文件集合
>        List<DownloadObjectInfo> needDownloadFiles = new List<DownloadObjectInfo>();
>
>        string url = "http://127.0.0.1/test.txt";   // 需要下载的文件的网络路径
>        string localfile = "C:/test.txt";           // 保存在本地的路径
>        DownloadObjectInfo download1 = new DownloadObjectInfo(url, localfile);
>        needDownloadFiles.Add(download1);
>
>        DownloadFilesRequest request = DownloadFilesRequest.DownloadFiles(needDownloadFiles);
>        while (!request.isDone)
>        {
>            yield return null;
>            Debug.LogFormat("进度:{0} 网速:{0} ",request.progress, request.Speed);
>        }
>        if (string.IsNullOrEmpty(request.error))
>        {
>            Debug.LogFormat("文件下载成功!");
>        }
>        else {
>            Debug.LogFormat("文件下载失败:{0}",request.error);
>        }
>    }
>}
> ```