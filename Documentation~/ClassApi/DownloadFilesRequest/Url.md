# DownloadFileRequest.Url



### 属性:

public string Url;

### 说明:

当前正在下载的文件地址

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UrlExample : MonoBehaviour
>{
>        IEnumerator Start()
>        {
>            DownloadFileRequest request = DownloadFileRequest.Download("url", "C:/test.txt");
>
>            // 下载的文件的地址
>            Debug.LogFormat("url:{0}",request.Url);
>
>            while (!request.isDone)
>            {
>                yield return null; 
>            }
>
>            if (string.IsNullOrEmpty(request.error))
>            {
>                // 文件下载成功
>            }
>            else {
>                // 文件下载失败
>                Debug.LogFormat("文件下载失败:{0}",request.error);
>            }
>        }
>}
> ```