# DownloadFilesRequest.Abort



### 方法:

public void Abort();

### 说明:

中断下载

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class AbortExample : MonoBehaviour
>{
>    void Start()
>    { 
>        // 需要下载的文件集合
>        List<DownloadObjectInfo> needDownloadFiles = new List<DownloadObjectInfo>();
>
>        string url = "http://127.0.0.1/test.txt";   // 需要下载的文件的网络路径
>        string localfile = "C:/test.txt";           // 保存在本地的路径
>        DownloadObjectInfo download1 = new DownloadObjectInfo(url, localfile);
>        needDownloadFiles.Add(download1);
>
>        DownloadFilesRequest request = DownloadFilesRequest.DownloadFiles(needDownloadFiles);
>
>        // 中断下载
>        request.Abort();
>    }
>}
> ```