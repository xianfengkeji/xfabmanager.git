# CoroutineStarter
class in XFABManager

### 说明:

协程启动类(适用于非继承自MonoBehaviour的脚本启动协程)

 

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [Start](CoroutineStarter/Start.md)      | 启动协程（适用于在非继承自MonoBehaviour的脚本中启动协程）,且通过该方法启动的协程不会因为切换场景而停止        |
| [Stop](CoroutineStarter/Stop.md)      | 停止由CoroutineStarter启动的协程        |