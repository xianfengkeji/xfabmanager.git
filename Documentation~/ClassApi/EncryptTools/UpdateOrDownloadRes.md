# AssetBundleManager.UpdateOrDownloadRes



### 方法:

public static UpdateOrDownloadResRequest UpdateOrDownloadRes(string projectName);

### 说明:

更新或下载某个模块资源


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **UpdateOrDownloadResRequest**     **[可挂起](ReadyRes.md#返回值)**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |
| CurrentSpeed      | 下载速度 单位:字节    |
| CurrentSpeedFormatStr      | 格式化之后的下载速度 例如 : 100kb/s 或 10mb/s    |
| DownloadedSize      | 已下载的文件大小    |
| NeedDownloadedSize      | 需要下载的大小    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class UpdateOrDownloadResExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        UpdateOrDownloadResRequest request = AssetBundleManager.UpdateOrDownloadRes("Test");    
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 资源更新 或 下载成功
>        }
>        else 
>        {
>            Debug.LogFormat("更新或下载资源失败:{0}",request.error);    
>        } 
>    }
>}
> ```


### 重载方法:


public static UpdateOrDownloadResRequest UpdateOrDownloadRes(CheckUpdateResult result);

### 说明:


根据检测结果来更新或下载资源

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| result      | CheckUpdateResult 检测更新的结果        |
