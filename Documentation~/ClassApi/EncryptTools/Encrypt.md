# EncryptTools.Encrypt



### 方法:

public static byte[] Encrypt(byte[] data, string key);

### 说明:

通过DES加密算法对字节数组进行加密


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| data      | 需要加密的字节数组        |
| key      | 密钥        |

### 返回值

类型 : **byte[]** 

字段介绍 :  加密之后的字节数组

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class EncryptExample : MonoBehaviour
>{
>    void Start()
>    {
>        byte[] a = new byte[] {0,1,2,3 };
>        string key = "12345678"; 
>        // 对字节数组a进行加密，密钥:12345678
>        byte[] b = EncryptTools.Encrypt(a, key);
>        // 加密之后的字节数组b
>        Debug.Log(b.ToString());
>    }
>}
> ```


### 重载方法:


public static string Encrypt(string data, string key);

### 说明:


通过DES加密算法对字符串进行加密

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| data      | 需要加密的字符串        |
| key      | 密钥        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class EncryptExample : MonoBehaviour
>{
>    void Start()
>    {
>        string a = "hello xfabmanager";
>        string key = "12345678"; 
>        // 对字符串a进行加密，密钥:12345678
>        string b = EncryptTools.Encrypt(a, key);
>        // 加密之后的字符串b
>        Debug.Log(b);
>    }
>}
> ```
