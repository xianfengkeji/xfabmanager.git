# EncryptTools.Decrypt



### 方法:

public static byte[] Decrypt(byte[] data, string key);

### 说明:

通过DES算法对字节数组进行解密


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| data      | 需要解密的字节数组        |
| key      | 密钥        |

### 返回值

类型 : **byte[]**  

字段介绍 : 解密之后的字节数组


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DecryptExample : MonoBehaviour
>{
>    void Start()
>    {
>        byte[] a = new byte[] {0,1,2,3 };
>        string key = "12345678"; 
>        // 对字节数组a进行解密，密钥:12345678
>        byte[] b = EncryptTools.Decrypt(a, key);
>        // 解密之后的字节数组b
>        Debug.Log(b.ToString());
>    }
>}
> ```


### 重载方法:


public static string Decrypt(string data, string key);

### 说明:


通过DES算法对字符串进行解密

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| data      | 需要解密的字符串        |
| key      | 密钥        |


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DecryptExample : MonoBehaviour
>{
>    void Start()
>    {
>        string a = "hello xfabmanager!";
>        string key = "12345678"; 
>        // 对字符串a进行解密，密钥:12345678
>        string b = EncryptTools.Decrypt(a, key);
>        // 解密之后的字符串b
>        Debug.Log(b.ToString());
>    }
>}
> ```