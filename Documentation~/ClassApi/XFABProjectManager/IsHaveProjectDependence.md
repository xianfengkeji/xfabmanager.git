# XFABProjectManager.IsHaveProjectDependence



### 方法:

public bool IsHaveProjectDependence(string name);

### 说明:

是否有项目依赖这个项目


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| name      | 资源模块名称        |

### 返回值

类型 : **boolean**  

字段介绍 : 
 
true:有 false:没有

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsHaveProjectDependenceExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 判断当前项目中是否有项目依赖名为Test的项目
>        XFABProjectManager.Instance.IsContainProject("Test");
>    }
>}
> ```