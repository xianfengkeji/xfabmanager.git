# XFABProjectManager.GetProject



### 方法:

public XFABProject GetProject(string name);

### 说明:

查询项目


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| name      | 资源模块名称        |

### 返回值

类型 : **XFABProject**  

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| name      | 资源项目名称  ![](textures/name.jpeg)       |
| displayName      | 显示名称 ![](textures/display_name.jpeg)      |
| assetBundles      | 当前项目创建的所有AB包 ![](textures/bundlelist.jpeg)       |
| dependenceProject      | 依赖的项目名称的集合 ![](textures/dependenceProject.jpeg)     |
| suffix      | AssetBundle资源文件后缀（默认.unity3d） ![](textures/suffix.jpeg)    |
| version      | 当前资源模块的版本 ![](textures/version.jpeg)   |
| isClearFolders      | 在打包资源时是否清空文件夹 ![](textures/clearfolders.jpeg)    |
| isCopyToStreamingAssets      | 打包完成后是否把资源复制到StreamingAssets目录 ![](textures/streamingassets.jpeg)    |
| isCompressedIntoZip      | 打包完成后是否把资源压缩成压缩包 ![](textures/zip.jpeg)    |
| isAutoPackAtlas      | 是否自动打包图集（默认true） ![](textures/atlas.jpeg)     |
| atlas_compression_type      | 图集压缩类型 ![](textures/compression.jpeg)    |
| use_crunch_compression      | 是否使用Crunch压缩 ![](textures/crunch_compression.jpeg)     |
| compressor_quality      | 压缩质量（范围:0-100,默认50） ![](textures/compressorQuality.jpeg)    |
| update_message      | 更新信息 ![](textures/update_message.jpeg)   |
| update_date      | 打包资源的日期   |
| buildAssetBundleOptions      | AssetBundle打包选项 ![](textures/buildassetbundleoptions.jpeg)   |
| secret_key      | 密钥 ![](textures/secret.jpeg)   |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetProjectExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 查询名为Test的资源模块
>        XFABProject project = XFABProjectManager.Instance.GetProject("Test");
>        if (project != null)
>        {
>            // 查询成功
>        }
>        else { 
>            // 查询失败
>        }
>    }
>}
> ```