# AssetBundleManager.IsContainProject



### 方法:

public bool IsContainProject(string name);

### 说明:

判断是否包含某个项目


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| projectName      | 资源模块名称        |

### 返回值

类型 : **boolean** 

字段介绍 :

true:包含 false:不包含

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsContainProjectExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 判断当前项目是否有名为Test的资源模块
>        Debug.LogFormat("IsContainProject:{0}", XFABProjectManager.Instance.IsContainProject("Test"));
>    }
>}
> ```