# XFABProjectManager.RefreshProjects



### 方法:

public void RefreshProjects();

### 说明:

刷新项目列表


**注:创建或者删除资源模块时会调用，开发者一般使用不到!**

 
### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class RefreshProjectsExample : MonoBehaviour
>{
>    void Start()
>    {
>        XFABProjectManager.Instance.RefreshProjects();
>    }
>}
> ```