# XFABProjectManager.Projects



### 属性:

public List<*XFABProject*> Projects;

### 说明:

所有资源模块集合


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class ProjectsExample : MonoBehaviour
>{
>    void Start()
>    {
>        Debug.LogFormat("资源模块数量:{0}", XFABProjectManager.Instance.Projects.Count);
>        foreach (var item in XFABProjectManager.Instance.Projects)
>        {
>            Debug.LogFormat("资源模块名称:{0}",item.name);
>        }
>    }
>}
> ```
