# RemoteFileInfoRequest
class in XFABManager

### 说明:

获取网络文件信息(比如:文件大小，最后的修改时间，是否存在 等)
 
### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [RequestInfo](RemoteFileInfoRequest/RequestInfo.md)      | 请求网络文件信息        |