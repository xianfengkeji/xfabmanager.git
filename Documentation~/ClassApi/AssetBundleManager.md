# AssetBundleManager
class in XFABManager

### 说明:

使用 AssetBundleManager 可以实现资源加载，卸载 更新 释放 下载 准备 等功能!

另外还可以通过此类 修改项目热更新地址 清理本地资源缓存 获取本地缓存大小 设置解密密钥 设置自定义版本获取方式 设置自定义服务器文件路径拼接方式 等!

### 静态属性

| 名称      | 说明 |
| ----------- | ----------- |
|  [AssetBundles](AssetBundleManager/AssetBundles.md)  | 已加载的AssetBundle       |
|  [BundleLoadedAssets](AssetBundleManager/BundleLoadedAssets.md)  | 记录某个AssetBundle中加载了哪些资源      |


### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [ReadyRes](AssetBundleManager/ReadyRes.md)      | 准备某个模块的资源        |
| [CheckResUpdate & CheckResUpdates](AssetBundleManager/CheckResUpdates.md)      | 检测某个项目 及其依赖项目 是否需要更新 下载 或者 释放 等等        |
| [UpdateOrDownloadRes](AssetBundleManager/UpdateOrDownloadRes.md)      | 更新或下载某个模块资源        |
| [ExtractRes](AssetBundleManager/ExtractRes.md)      | 释放AssetBunle(从StreamingAssets复制到persistentDataPath),释放当前项目资源,不包含其依赖项目 |
| [GetAssetBundleSuffix](AssetBundleManager/GetAssetBundleSuffix.md)      |  获取某个模块 AssetBundle 的后缀名 |
| [IsLoadedAssetBundle](AssetBundleManager/IsLoadedAssetBundle.md)      |  判断是否已经加载 某个AssetBundle,如果是编辑器模式并且从Assets加载资源会一直返回True |
| [LoadAllAssetBundles](AssetBundleManager/LoadAllAssetBundles.md)      |  异步加载某个模块所有的AssetBundle |
| [UnLoadAllAssetBundles](AssetBundleManager/UnLoadAllAssetBundles.md)      | 卸载某个 Project 的所有AssetBundle |
| [LoadAllAssets](AssetBundleManager/LoadAllAssets.md)      | 加载某个AssetBundle的所有资源 |
| [LoadAllAssetsAsync](AssetBundleManager/LoadAllAssetsAsync.md)      | 加载某个AssetBundle的所有资源异步 |
| [LoadAsset](AssetBundleManager/LoadAsset.md)      | 加载资源 |
| [LoadAssetAsync](AssetBundleManager/LoadAssetAsync.md)      | 异步加载资源 |
| [LoadSubAsset](AssetBundleManager/LoadSubAsset.md)      | 加载子资源 |
| [LoadSubAssetAsync](AssetBundleManager/LoadSubAssetAsync.md)      | 异步加载子资源 |
| [LoadAssetWithSubAssets](AssetBundleManager/LoadAssetWithSubAssets.md)      | 加载资源及所有子资源 |
| [LoadAssetWithSubAssetsAsync](AssetBundleManager/LoadAssetWithSubAssetsAsync.md)      | 异步加载资源及所有子资源 |
| [LoadScene](AssetBundleManager/LoadScene.md)      | 加载打包在AssetBundle中的场景 |
| [LoadSceneAsync](AssetBundleManager/LoadSceneAsync.md)      | 异步加载打包在AssetBundle中的场景(已过时) |
| [LoadSceneAsynchrony](AssetBundleManager/LoadSceneAsynchrony.md)      | 异步加载打包在AssetBundle中的场景 |
| [UnloadAsset](AssetBundleManager/UnloadAsset.md)      | 卸载资源 |
| [IsHaveBuiltInRes](AssetBundleManager/IsHaveBuiltInRes.md)      | 判断某个模块的资源是否内置在安装包中 |
| [IsHaveResOnLocal](AssetBundleManager/IsHaveResOnLocal.md)      | 判断本地是否有某个模块资源 |
| [SetGetProjectVersion](AssetBundleManager/SetGetProjectVersion.md)      | 自定义获取版本的接口 |
| [SetServerFilePath](AssetBundleManager/SetServerFilePath.md)      | 设置服务端文件路径接口 |
| [GetProjectDependencies](AssetBundleManager/GetProjectDependencies.md)      | 获取某个项目的依赖项目 |
| [GetAssetBundleDependences](AssetBundleManager/GetAssetBundleDependences.md)      | 获取AssetBundle的依赖 |
| [ExecuteOnlyOnceAtATime](AssetBundleManager/ExecuteOnlyOnceAtATime.md)      | 执行一个异步请求,如果该异步请求已经在执行中,则返回执行中的请求对象,该异步请求必须继承CustomYieldInstruction |
| [GetAssetBundleFilesSize](AssetBundleManager/GetAssetBundleFilesSize.md)      | 获取某一个模块的所有AssetBundle文件大小 单位:字节 |
| [DeleteAssetBundleFiles](AssetBundleManager/DeleteAssetBundleFiles.md)      | 删除某一个模块的所有AssetBundle文件,文件删除后资源将无法加载，若想继续使用请重新准备资源(释放或下载等) |
| [SetProjectUpdateUrl](AssetBundleManager/SetProjectUpdateUrl.md)      | 手动设置某一个项目的更新地址 如果 projectName为空 则设置所有的项目的url |
| [GetProfile](AssetBundleManager/GetProfile.md)      | 获取某一个模块的配置信息 (如果没有单独设置则返回默认配置) |
| [GetAssetBundleInfo](AssetBundleManager/GetAssetBundleInfo.md)      | 获取当前已加载的资源信息( 可根据此信息查看AssetBundle的释放情况 ) |
| [SetSecret](AssetBundleManager/SetSecret.md)      | 设置某一个模块的资源解密密钥 |
| [GetSecret](AssetBundleManager/GetSecret.md)      | 获取某个模块的资源解密密钥 |