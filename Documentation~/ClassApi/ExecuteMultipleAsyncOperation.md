# ExecuteMultipleAsyncOperation
class in XFABManager

### 说明:

同时执行多个异步请求

### 事件
| 名称      | 说明 |
| ----------- | ----------- |
| onAsyncOperationCompleted  | 当某一个异步操作执行完成时触发，参数:异步操作的对象      | 

### 字段
| 名称      | 说明 |
| ----------- | ----------- |
| InExecutionAsyncOperations  | 正在执行中的异步操作集合      | 

### 构造函数

| 名称      | 说明 |
| ----------- | ----------- |
| ExecuteMultipleAsyncOperation(int max_concurrent_execution_count)  | 参数:最多同时执行的异步请求的数量       | 


### 方法

| 名称      | 说明 |
| ----------- | ----------- |
| Add(T instruction)      | 加入一个正在执行中的异步请求        |
| CanAdd()      | 判断是否能够执行更多的异步请求，如果返回true则说明可以继续加入        |
| IsDone()      | 判断异步请求是否全部执行完成        |
| Error()      | 请求的错误信息，如果为空则说明没有出错        |

### 代码示例

假如我们要下载100个文件，如果一个下载完成再去下载另外一个，这样就太慢了，所以我们可以多个文件同时下载，具体代码如下:


 > ```none
>
>using UnityEngine;
>
>public class ExecuteMultipleAsyncOperationExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        // 待下载的文件 假如有100个
>        Queue<DownloadObjectInfo> waitDownloadObjects = new Queue<DownloadObjectInfo>();
>        // 10 个异步请求同时执行
>        ExecuteMultipleAsyncOperation<DownloadFileRequest> multiple_download_operation = new ExecuteMultipleAsyncOperation<DownloadFileRequest>(10);
>
>        while (waitDownloadObjects.Count > 0 || !multiple_download_operation.IsDone())
>        {
>            // 可以下载
>            while (multiple_download_operation.CanAdd() && waitDownloadObjects.Count > 0)
>            {
>                // 开始下载
>                DownloadObjectInfo info = waitDownloadObjects.Dequeue();
>                if (string.IsNullOrEmpty(info.url) || string.IsNullOrEmpty(info.localfile)) continue;
>                DownloadFileRequest download = DownloadFileRequest.Download(info.url, info.localfile); 
>                // 加入异步请求
>                multiple_download_operation.Add(download);
>            }
>
>            yield return null;
>
>            // 判断是否出错
>            if (!string.IsNullOrEmpty(multiple_download_operation.Error())) break; // 有文件下载失败
>        }
>
>        // 等待正在下载的其他任务执行完成
>        while (!multiple_download_operation.IsDone())
>        {
>            yield return null;
>        }
>
>        // 检测文件是否下载出错
>        if (!string.IsNullOrEmpty(multiple_download_operation.Error()))
>        {
>            Debug.LogFormat("文件下载出错:{0}", multiple_download_operation.Error());
>        }
>        else 
>        {
>            // 文件下载完成
>        }
>    }
>}
> ```