# EncryptTools
class in XFABManager

### 说明:

加密工具类

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [Encrypt](EncryptTools/Encrypt.md)      | DES加密       |
| [Decrypt](EncryptTools/Decrypt.md)      | DES解密       | 