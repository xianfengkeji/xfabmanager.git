# FileTools.GetDirectorySize



### 方法:

public static long GetDirectorySize(string dirPath);

### 说明:

获取文件夹大小(可在编辑器非运行状态下调用)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| dirPath      | 希望获取大小的文件夹路径        |

### 返回值

类型 : **long** 

字段介绍 : 文件夹占用空间的大小，单位：字节

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class GetDirectorySizeExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 获取C:/test这个文件占用空间的大小
>        long size = FileTools.GetDirectorySize("C:/test");
>        Debug.LogFormat("size:{0}", size);
>    }
>}
> ```
 