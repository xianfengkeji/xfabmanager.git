# FileTools.Copy



### 方法:

public static CopyFileRequest Copy(string source, string des);

### 说明:

异步复制一个文件(可用于Android平台下的StreamingAssets目录文件复制,不可在编辑器非运行状态下调用)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| source      | 需要复制的源文件路径        |
| des      | 目标路径        |

### 返回值

类型 : **CopyFileRequest**

字段介绍 : 

| 名称        | 说明 |
| ----------- | ----------- |
| completed      | 执行完成的事件 , 异步请求执行结束时触发!       |
| isDone      | 是否执行完成的属性, 可用于判断当前的异步请求是否执行完成        |
| progress      | 当前异步请求的进度,范围 0-1       |
| error      | 当前的异步请求执行中是否出错,如果error为空 ,并且已经执行完成 ，说明该请求执行成功!    |

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CopyExample : MonoBehaviour
>{
>    IEnumerator Start()
>    {
>        string source = "C:/test.txt";
>        string target = "C:/test1.txt";
>        //把文件C:/test.txt 复制到 C:/test1.txt
>        CopyFileRequest request = FileTools.Copy(source, target);
>        yield return request;
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 文件复制成功
>        }
>        else {
>            Debug.LogFormat("文件复制失败:{0}",request.error);
>        }
>    }
>}
> ```