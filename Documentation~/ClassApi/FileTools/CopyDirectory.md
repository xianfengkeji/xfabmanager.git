# FileTools.CopyDirectory



### 方法:

public static bool CopyDirectory(string sourceDir, string destDir, Action<string, float> progress,string[] excludeSuffix = null);

### 说明:

复制一个文件夹(可在编辑器非运行状态下调用)


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| sourceDir      | 希望复制的文件夹路径        |
| destDir      | 目标路径        |
| progress      | 进度改变的回调 string:正在复制的文件名称 float:进度(0-1)        |
| excludeSuffix      | 不需要复制的文件的后缀        |

### 返回值

类型 : **boolean**  

字段介绍 : true:可正常复制 false:不可正常复制

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class CopyDirectoryExample : MonoBehaviour
>{
>    void Start()
>    {
>        // 复制文件夹:C:/Users/lixueliao/Desktop/素材 到 C:/Users/lixueliao/Desktop/素材1
>        FileTools.CopyDirectory("C:/Users/lixueliao/Desktop/素材", "C:/Users/lixueliao/Desktop/素材1", (name, progress) =>
>        { 
>            Debug.LogFormat("name:{0} progress:{1}", name, progress); 
>        },null); 
>    }
>}
> ```