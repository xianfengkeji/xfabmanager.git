# XFABTools
class in XFABManager

### 说明:

XFABManager工具类,提供一些公用的方法和功能!

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [DataPath](XFABTools/DataPath.md)      | 数据存放目录        |
| [BuildInDataPath](XFABTools/BuildInDataPath.md)      | 内置数据目录(安装包中的数据的目录)         |
| [LocalResPath](XFABTools/LocalResPath.md)      | 本地资源文件路径       |
| [GetCurrentPlatformName](XFABTools/GetCurrentPlatformName.md)      | 获取当前平台的名称      |
| [md5](XFABTools/md5.md)      | 计算字符串的MD5值 |
| [md5file](XFABTools/md5file.md)      | 计算文件的MD5值 |
| [CaculateFileMD5](XFABTools/CaculateFileMD5.md)      |  异步计算md5的值,如果文件过大推荐使用此方法 |
| [IsBaseByClass](XFABTools/IsBaseByClass.md)      |  判断一个类是否继承另外一个类(如果传入两个相同的类会返回false)|
| [IsImpInterface](XFABTools/IsImpInterface.md)      | 判断一个类是否实现了某个接口 |
| [GetLocalVersion](XFABTools/GetLocalVersion.md)      | 查询本地某个模块缓存的资源版本号 |