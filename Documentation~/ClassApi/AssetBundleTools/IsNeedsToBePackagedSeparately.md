# AssetBundleTools.IsNeedsToBePackagedSeparately



### 方法:

public static bool IsNeedsToBePackagedSeparately(string asset_path);

### 说明:

判断一个资源是不是需要单独打包
    
暂时认为 .prefab .ttf .fontsettings .asset .TTF 结尾的文件单独打包 或者单个文件大小大于1MB

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| asset_path      |  资源路径(例如:Assets/xxx/xxx.txt)          |

### 返回值

类型 : **boolean** 

字段介绍 : true:需要单独打包 false:不需要单独打包

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsNeedsToBePackagedSeparatelyExample : MonoBehaviour
>{
>    void Start()
>    {
>#if UNITY_EDITOR
>        string asset_path = "Assets/test.txt";
>        bool separately = AssetBundleTools.IsNeedsToBePackagedSeparately(asset_path);
>        Debug.LogFormat("separately:{0}", separately);
>#endif 
>    }
>}
> ```