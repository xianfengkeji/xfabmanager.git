# AssetBundleTools.IsValidAssetBundleFile



### 方法:

public static bool IsValidAssetBundleFile(string asset_path);

### 说明:

判断一个资源文件是否能够打包进AssetBundle(仅在编辑器模式可用)

### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| asset_path      | 资源路径(例如:Assets/xxx/xxx.txt)        |

### 返回值

类型 : **boolean**  

字段介绍 :  true:可以打进AssetBundle false:不能打进AssetBundle


### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class IsValidAssetBundleFileExample : MonoBehaviour
>{
>    void Start()
>    {
>#if UNITY_EDITOR
>        string asset_path = "Assets/test.txt";
>        bool isValid = AssetBundleTools.IsValidAssetBundleFile(asset_path);
>        Debug.LogFormat("isValid:{0}", isValid);
>#endif 
>    }
>}
> ```

