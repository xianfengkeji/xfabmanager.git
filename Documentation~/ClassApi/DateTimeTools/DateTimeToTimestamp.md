# DateTimeTools.DateTimeToTimestamp



### 方法:

public static long DateTimeToTimestamp(DateTime datetime,bool millisecond = true);

### 说明:

本时区日期时间转时间戳


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| datetime      | 日期        |
| millisecond      | true:毫秒 false:秒 默认:毫秒        |

### 返回值

类型 : **long**  

字段介绍 : 

毫秒或者秒的时间戳

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class DateTimeToTimestampExample : MonoBehaviour
>{
>    void Start()
>    {
>        Debug.LogFormat("当前的时间戳:{0}", DateTimeTools.DateTimeToTimestamp(DateTime.Now)) ;
>    }
>}
> ```