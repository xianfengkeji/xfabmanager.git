# DateTimeTools.TimestampToDateTime



### 方法:

public static DateTime TimestampToDateTime(long timeStamp, bool millisecond = true);

### 说明:

时间戳转本时区日期时间


### 参数

| 名称        | 说明 |
| ----------- | ----------- |
| datetime      | 时间戳        |
| millisecond      | true:毫秒 false:秒 默认:毫秒        |

### 返回值

类型 : **DateTime**  

字段介绍 : 

日期

### 代码示例:

> ```none
>
>using UnityEngine;
>
>public class TimestampToDateTimeExample : MonoBehaviour
>{
>    void Start()
>    { 
>        // 把当前日期转成时间戳
>        long timestamp = DateTimeTools.DateTimeToTimestamp(DateTime.Now);
>        // 把时间戳转成日期
>        DateTime dateTime = DateTimeTools.TimestampToDateTime(timestamp);
>        Debug.LogFormat("日期:{0}",dateTime.ToString());
>    }
>}
> ```