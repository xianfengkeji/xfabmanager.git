# GameObjectLoader
class in XFABManager

### 说明:

加载并且实例化GameObject的类，并且对GameObject的加载进行管理和引用计数,当引用为0时会自动卸载资源(推荐使用) 

### 静态属性

| 名称      | 说明 |
| ----------- | ----------- |
| Instance      | GameObjectLoader实例（单例） (已过时,方法已改为静态方法,请直接通过类名调用)      |

### 静态方法

| 名称      | 说明 |
| ----------- | ----------- |
| [Load](GameObjectLoader/Load.md)      | 加载预制体并进行实例化       |
| [LoadAsync](GameObjectLoader/LoadAsync.md)      | 异步加载预制体并进行实例化       |
| [UnLoad](GameObjectLoader/UnLoad.md)      | 回收预制体的实例       |