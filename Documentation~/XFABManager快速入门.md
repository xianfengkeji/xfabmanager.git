## XFABManager快速入门


### 功能介绍

XFABManager提供了AssetBundle的可视化管理功能，我们通过该插件可以很方便的对项目中的AssetBundle进行打包，添加文件，删除文件等等!!
除此之外，此插件还提供了AssetBundle的加载，卸载，更新，下载，压缩，释放等功能，通过该插件可以很方便快速的完成AssetBundle相关的功能开发，
提升开发效率!

### 插件特点

该插件管理资源是以模块为单位，一个项目中可以有多个模块的资源，模块与模块之间相互独立!

### 加载资源流程
在加载资源之前，首先需要添加资源，因为该插件管理资源是以模块为单位，所以我们需要创建一个资源模块，
然后把资源添加到模块中，如下:

#### 创建资源模块

1. 打开项目管理界面 (Window/XFKT/XFABManager/Projects)

![](textures/quick_start/quick_start_1.jpeg)

2. 此时可以打开如下界面:

![](textures/quick_start/quick_start_2.jpeg)

3. 点击上图中的"+"，就可以打开创建资源模块的界面，如下图:

![](textures/quick_start/quick_start_3.jpeg)

4. 在上图中填写对应的信息，点击创建按钮，此时会弹出选择文件夹的窗口，该文件夹是用来存放资源模块配置文件的，选择对应文件夹即可

![](textures/quick_start/quick_start_4.jpeg)

5. 文件夹选择完成后，资源模块就创建完成了，可在对应的文件夹下看到资源模块的配置文件，如下图:

![](textures/quick_start/quick_start_6.jpeg)

6. 同时在项目列表界面也可以看到新建的资源模块

![](textures/quick_start/quick_start_5.jpeg)

到这里资源模块的创建就完成了，接下来我们需要在资源模块中创建AssetBundle,添加资源，具体操作如下:

#### 添加资源

1. 打开资源模块的管理界面，在资源模块列表界面点击对应模块即可。

![](textures/quick_start/quick_start_7.jpeg)

2. 点击后，我们可以打开如下界面: 

   在下图中有三个页签，分别为AssetBundles,Build,Info。

   其中AssetBundles页签是用来管理当前模块中的AssetBundle,添加，删除，包括AssetBundle中的资源的添加和删除

   Build页签是用来打包AssetBundle（后面的文档会详细介绍）

   Info页签是用来管理当前资源模块的信息，名称，版本号，后缀，密钥等信息

![](textures/quick_start/quick_start_8.jpeg)

3. 创建AssetBundle,在上图中左侧的AssetBundle列表区域点击右键，会弹出一个菜单选项，如下图：

![](textures/quick_start/quick_start_9.jpeg)

4. 我们选择创建bundle,创建完成如下图:

![](textures/quick_start/quick_start_10.jpeg)

5. 给新创建bundle添加资源

![](textures/quick_start/quick_start_11.jpeg)
![](textures/quick_start/quick_start_12.jpeg)

#### 加载资源

1. 在前的步骤中，我们创建了资源模块并且添加了资源，接下我们来加载资源，加载资源的代码如下:

> ```none
>
>using UnityEngine;
>
>public class LoadAssetExample : MonoBehaviour
>{
>    void Start()
>    {
>        GameObject prefab = AssetBundleManager.LoadAsset<GameObject>("Test","Cube");
>        Texture texture = AssetBundleManager.LoadAsset<Texture>("Test", "Screenshot");
>    }
>}
> ```

**关于加载资源和AssetBundleManager的其他用法可以参考[AssetBundleManager的API文档](ClassApi/AssetBundleManager.md)**

**通过上面的代码，我们就可以加载添加到资源模块中Cube预制体和图片Screenshot。**

**但是此时如果您打包项目，会发现在真实的运行环境中资源加载会失败，因为当前加载资源的模式为Assets,资源加载模式设置如下图:**

![](textures/quick_start/quick_start_13.jpeg)
![](textures/quick_start/quick_start_14.jpeg)

**所以，如果在实际的运行环境中，我们需要从AssetBundle文件中加载资源，那怎样打包AssetBundle文件呢？可以参考下面的文档!**

### 生成AssetBundle文件

1. 打开资源模块管理界面的Build页签，如下图:

![](textures/quick_start/quick_start_15.jpeg)

2. 打包完成后可在Assets同级目录AssetBundles中找到对应的资源，如下图:

![](textures/quick_start/quick_start_16.jpeg)

**AssetBundle文件打包完成之后，我们需要在实际运行时读取并加载。那我们怎样获取到AssetBundle文件呢？一般呢有两种方式。**

**第一种:直接把AssetBundle文件放到安装包中，程序运行时去安装包中读取。我们称之为*内置***

**第二种:把AssetBundle文件放到服务端，程序运行时把AssetBundle文件从远程下载到本地，需要时去本地读取。**

那**内置**或者**从远程下载**应该怎样操作呢？可以参考下方的文档!

#### 文件目录介绍
| 名称        | 说明 |
| ----------- | ----------- |
| 内置目录      | StreamingAssets目录(只读)，该目录下的资源会原封不动的打包进安装包，所以可以把需要内置的资源放到该目录，但是该目录只能读，不能写，如果有热更的需求，热更下载下来的文件需要放到一个可读可写的目录，也就是数据目录     |
| 数据目录      | PersistentData目录(可读可写)，该目录在程序安装时由系统创建，卸载时会被系统删除，而且可读可写适合做数据目录       | 

#### 特定名词介绍
| 名称        | 说明 |
| ----------- | ----------- |
| 检测更新      | 根据资源更新模式，内置目录，数据目录的资源来判断当前模块的资源需要什么样的操作，例如:更新模式为内置，内置目录有资源，数据目录没有资源，检测的结果则为释放，此时需要把内置目录的资源复制到数据目录进行使用(仅针对内置目录不能直接读取的平台) [检测更新的详细规则](ImplementationMode/CheckUpdateLogic.md)      |
| 更新或下载      | 从服务端把新的AssetBundle文件下载到数据目录        |
| 释放资源      | 把内置目录的资源复制到数据目录        |
| 准备资源      | 先检测资源，根据检测的结果来做相应的处理，相当于自动化的处理。如果某一个资源模块准备成功后就说明该模块的资源已经是最新的并且可用的。        |


### 资源内置模式操作流程

1. 设置资源模块的更新模式为内置

![](textures/quick_start/quick_start_13.jpeg)
![](textures/quick_start/quick_start_17.jpeg)

2. 把资源复制到StreamingAssets 目录

![](textures/quick_start/quick_start_18.jpeg)

复制成功后可以下图的目录中看到对应的资源:

![](textures/quick_start/quick_start_19.jpeg)

3. 调用ReadyRes准备资源，资源准备成功后就可以正常使用加载了
   
> ```none
>
>using UnityEngine;
>
>public class LoadAssetExample : MonoBehaviour
>{
>    IEnumerator Start()
>    { 
>        ReadyResRequest request = AssetBundleManager.ReadyRes("Test"); 
>        yield return request; 
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 资源准备成功 加载您想加载的资源 
>            GameObject prefab = AssetBundleManager.LoadAsset<GameObject>("Test", "Cube");
>            Texture texture = AssetBundleManager.LoadAsset<Texture>("Test", "Screenshot");
>        }
>        else {
>            Debug.LogFormat("资源准备失败:{0}",request.error);
>        } 
>    }
>}
> ```

   **当然您也可以直接调用释放资源，代码如下:**

> ```none
>
>using UnityEngine;
>
>public class LoadAssetExample : MonoBehaviour
>{
>    IEnumerator Start()
>    { 
>        ExtractResRequest request = AssetBundleManager.ExtractRes("Test"); 
>        yield return request; 
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 释放资源成功 加载您想加载的资源 
>            GameObject prefab = AssetBundleManager.LoadAsset<GameObject>("Test", "Cube");
>            Texture texture = AssetBundleManager.LoadAsset<Texture>("Test", "Screenshot");
>        }
>        else {
>            Debug.LogFormat("释放资源失败:{0}",request.error);
>        } 
>    }
>}
> ```

   但是我们并不推荐您这么做，因为释放资源仅仅只能满足非更新的模式，但是ReadyRes可以满足任何情况(推荐使用)。

   关于ReadyRes的更多用法可以参考:**[ReadyRes的API](ClassApi/AssetBundleManager/ReadyRes.md)**


### 资源更新模式操作流程

1. 把AssetBundle文件放在服务器上，保证资源文件能够通过网络链接访问!
   这里我在自己电脑上开一个iis服务器，我们把AssetBundle文件放上去。


![](textures/quick_start/quick_start_20.jpeg)

![](textures/quick_start/quick_start_21.jpeg)

![](textures/quick_start/quick_start_22.jpeg)


   *如果开启iis后文件无法下载，可能是因为没有为对应后缀的文件配置 MIME类型，具体配置方法请大家自行查阅资料，这里就不做说明了!*
 
2. 配置更新地址，如下图：

![](textures/quick_start/quick_start_13.jpeg)
![](textures/quick_start/quick_start_23.jpeg)

*当然这种方式为手动配置，我们也可以通过代码,在运行时设置热更的Url，参考API:[SetProjectUpdateUrl](ClassApi/AssetBundleManager/SetProjectUpdateUrl.md)*

3. 在更新之前，我们需要知道当前我们正在使用的是哪一个版本的资源，所以我们需要获取资源的版本号,

   默认的资源版本号获取的方式是去固定的路径读取：Url/versions/projectName/version.txt 。

   比如当前我们的
   
   url为http://127.0.0.1/AssetBundles,
   
   projectName是Test,所以版本号的地址为：

   http://127.0.0.1/AssetBundles/versions/Test/version.txt

   我们只需要在上面的地址文件中写入版本号即可!

![](textures/quick_start/quick_start_24.jpeg)

![](textures/quick_start/quick_start_25.jpeg)


   *当然在实际的项目开发中或者工作中，我们一般会通过一个Web的API接口来获取资源版本的信息，此时就需要自定义资源版本的获取了，具体可以参考:[SetGetProjectVersion](ClassApi/AssetBundleManager/SetGetProjectVersion.md)*


4. 调用ReadyRes即可，代码如下:

> ```none
>
>using UnityEngine;
>
>public class LoadAssetExample : MonoBehaviour
>{
>    IEnumerator Start()
>    { 
>        ReadyResRequest request = AssetBundleManager.ReadyRes("Test"); 
>        yield return request; 
>        if (string.IsNullOrEmpty(request.error))
>        {
>            // 资源准备成功 加载您想加载的资源 
>            GameObject prefab = AssetBundleManager.LoadAsset<GameObject>("Test", "Cube");
>            Texture texture = AssetBundleManager.LoadAsset<Texture>("Test", "Screenshot");
>        }
>        else {
>            Debug.LogFormat("资源准备失败:{0}",request.error);
>        } 
>    }
>}
> ```

*在更新模式下，我们同样不建议您使用**AssetBundleManager.UpdateOrDownloadRes**这个方法来更新资源，因为在更新模式下情况比较复杂，*

*需要做的操作也不仅仅只是**更新或下载**，也可能需要释放资源，所以推荐使用ReadyRes方法来准备资源，*

*关于ReadyRes的更多用法可以参考:***[ReadyRes的API](ClassApi/AssetBundleManager/ReadyRes.md)**

*注：更新模式也可以内置资源，在检测更新时如果检测到有内置的资源，会优先使用内置的资源，避免造成没有必要的下载!*


### 项目配置设置

1. 打开项目配置

![](textures/quick_start/quick_start_13.jpeg)

![](textures/quick_start/quick_start_26.jpeg)

2. 新建项目配置,点击鼠标右键，弹出下图的菜单选项:

![](textures/quick_start/quick_start_27.jpeg)


3. 因为在实际的项目开发中，可能会有不同的环境，比如开发环境，测试环境，正式环境，每个环境他的配置可能都是不一样的，
   比如热更地址，加载模式等等，这个时候我们就可以新建不同的配置组，来对应不同的环境!

![](textures/quick_start/quick_start_28.jpeg)


4. 在打包时我们选择对应的配置即可，比如打测试环境的包就选择Test配置组，如下图:

![](textures/quick_start/quick_start_29.jpeg)

   当然我们也可以使用代码来设置我们想要使用的组，具体参考:[SetTargetGroup](ClassApi/XFABManagerSettings/SetTargetGroup.md)

5. 除了新建组之外，我们也可以在某一个组中新建具体某一项配置，如下图:

![](textures/quick_start/quick_start_30.jpeg)

![](textures/quick_start/quick_start_31.jpeg)

### Loaders 教程(必读) 

| 名称        | 说明 |
| ----------- | ----------- |
| [GameObjectLoader](ClassApi/GameObjectLoader.md)      | 加载并且实例化GameObject的类，并且对GameObject的加载进行管理和引用计数,当引用为0时会自动卸载资源(推荐使用)           |
| [ImageLoader](ClassApi/ImageLoader.md)      | 图片加载器          |


### 如有疑问 或 遗漏请及时联系群主,qq交流群:946441033