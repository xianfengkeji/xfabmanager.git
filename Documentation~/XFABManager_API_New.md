# Runtime API

| 脚本      | 说明 |
| ----------- | ----------- |
| [AssetBundleManager](ClassApi/AssetBundleManager.md)     |  使用 AssetBundleManager 可以实现资源加载，卸载 更新 释放 下载 准备 等功能!       |
| [XFABTools](ClassApi/XFABTools.md)      | XFABManager工具类,提供一些公用的方法和功能!         |
| [ZipTools](ClassApi/ZipTools.md)      | 压缩和解压的工具类          |
| [FileTools](ClassApi/FileTools.md)      | 文件工具类          |
| [EncryptTools](ClassApi/EncryptTools.md)      | 加密工具类          |
| [AssetBundleTools](ClassApi/AssetBundleTools.md)      | AssetBundle工具类          |
| [XFABManagerSettings](ClassApi/XFABManagerSettings.md)      | 通过该类可以获取或修改插件配置,比如:显示模式，配置组等          |
| [DownloadFileRequest](ClassApi/DownloadFileRequest.md)      | 下载文件的请求类          |
| [DownloadFilesRequest](ClassApi/DownloadFilesRequest.md)      | 下载多个文件的请求类          |
| [CoroutineStarter](ClassApi/CoroutineStarter.md)      | 协程启动类(适用于非继承自MonoBehaviour的脚本启动协程)          |
| [GameObjectLoader](ClassApi/GameObjectLoader.md)      | 加载并且实例化GameObject的类，并且对GameObject的加载进行管理和引用计数,当引用为0时会自动卸载资源(推荐使用)           |
| [ImageLoader](ClassApi/ImageLoader.md)      | 图片加载器          |
| [ImageLoaderManager](ClassApi/ImageLoaderManager.md)      | ImageLoader帮助类          |
| [RemoteFileInfoRequest](ClassApi/RemoteFileInfoRequest.md)      | 获取网络文件信息(比如:文件大小，最后的修改时间，是否存在 等)          |
| [ExecuteMultipleAsyncOperation](ClassApi/ExecuteMultipleAsyncOperation.md)      | 同时执行多个异步请求          |
| [DateTimeTools](ClassApi/DateTimeTools.md)      | 日期工具          |
| [EditorApplicationTool](ClassApi/EditorApplicationTool.md)      | EditorApplication工具          |

# Editor API 

| 脚本      | 说明 |
| ----------- | ----------- | 
| [XFABProjectManager](ClassApi/XFABProjectManager.md)     | 资源模块管理类        |
| [ProjectBuild](ClassApi/ProjectBuild.md)     | 资源模块打包工具类        |
| [IPostprocessBuildProject](ClassApi/IPostprocessBuildProject.md)     | 某一个资源模块打包完成的事件监听(仅在Editor模式下可用)        |
| [IPreprocessBuildProject](ClassApi/IPreprocessBuildProject.md)     | 某一个资源模块打包前的事件监听(仅在Editor模式下可用)        |


# 如有疑问 或 遗漏请及时联系群主,qq交流群:946441033