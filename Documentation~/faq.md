# FAQ

## 资源请求异常:Insecure connection not allowed,Non-secure network connections disabled in Player Settings!
在新版本的Unity中新增了如下设置，

![](textures/faq/downloads_http.jpg)

该设置的默认选项为Not allowed,如果设置为Not allowed将无法通过UnityWebRequest发起Http的网络请求，此时仅能发起Https的网络请求，
如果您的网络资源路径是http开头的，将无法正常下载，此时您可以把此选项修改为Always allowed来解决此问题，也可以选择把资源放在https的路径下!

## WebGL平台 文件请求失败: No 'Access-Control-Allow-Origin' header is present on the requested resource'

跨域访问失败,解决方案可参考下面的文章:

https://blog.csdn.net/dear_little_bear/article/details/83999391

## 打包后场景光照贴图丢失

可以尝试关闭自动处理光照贴图的选项, Edit/Project Settings.../Graphics/Lightmap Modes 设置为Custom, 不要选择Automatic!

![](textures/faq/lightmap%20models.png)

## QQ交流群:946441033 碰到问题请及时联系群主!